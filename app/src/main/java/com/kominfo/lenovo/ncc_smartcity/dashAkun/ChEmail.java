package com.kominfo.lenovo.ncc_smartcity.dashAkun;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kominfo.lenovo.ncc_smartcity.R;
import com.kominfo.lenovo.ncc_smartcity._globalVariable.MessageVariabe;
import com.kominfo.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.kominfo.lenovo.ncc_smartcity.internalLib.CheckConn;
import com.kominfo.lenovo.ncc_smartcity.internalLib.RetrofitLib.base_url.Base_url;
import com.kominfo.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management.SendDataLogin;
import com.kominfo.lenovo.ncc_smartcity.internalLib.SessionCheck;
import com.kominfo.lenovo.ncc_smartcity.internalLib.TextSafety;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by USER on 26/04/2018.
 */

public class ChEmail extends AppCompatActivity {
    Button btnSave, btnCancle;
    EditText etEmail;

    TextSafety textSafety = new TextSafety();
    CheckConn checkConn = new CheckConn();

    String BASE_URL = URLCollection.DATA_SOURCE_LOKER;
    String TOKEN_MOBILE = "X00W";
    String TAG = "ChEmail";

    Call<ResponseBody> getdata;
    SendDataLogin base_url_management;

    ProgressDialog progressDialog;
    Intent intent;

    String user_id = null;

    SessionCheck dataSession = new SessionCheck();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dash_ch_email);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnCancle = (Button) findViewById(R.id.btnCan);

        etEmail = (EditText) findViewById(R.id.etEmailNew);

        //get session for user id
        String[] data = dataSession.seesionLoginChecker(this);
        if (data != null) {
            user_id = data[1];
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkConn.isConnected(ChEmail.this)) {
                    if (checkInput()) {
                        setData();
                    }
                }
            }
        });

        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private boolean checkInput() {
        if (textSafety.get_valid_email(etEmail.getText().toString()) && !etEmail.getText().toString().isEmpty()) {
            return true;
        }
        return false;
    }

    private void setData() {
        progressDialog = new ProgressDialog(ChEmail.this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };

        progressDialog.setMessage(MessageVariabe.MESSAGE_LOADING);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        base_url_management = Base_url.getClient_notGson(BASE_URL)
                .create(SendDataLogin.class);
        getdata = base_url_management.UpdateEmail(user_id
                , etEmail.getText().toString()
                , TOKEN_MOBILE
                , "2");

        setData(getdata);
    }

    public void setData(Call<ResponseBody> getdata) {
        Log.e(TAG, "adapterRequest: run");
        getdata.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                progressDialog.dismiss();

                JSONObject dataJson = null;
                String message_response = MessageVariabe.MESSAGE_ERROR_RESPONSE_FAIL_SERVER;
                try {
                    dataJson = new JSONObject(response.body().string());
                    String status = dataJson.getJSONObject("body_message").getString("status");
                    if (Boolean.valueOf(status)) {
                        message_response = dataJson.getJSONObject("body_message").getString("message");
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e.getMessage());
                    message_response = MessageVariabe.MESSAGE_EMAIL_CHECK;
                }
                Toast.makeText(ChEmail.this, message_response, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ChEmail.this, MessageVariabe.MESSAGE_ERROR_RESPONSE_FAIL_SERVER, Toast.LENGTH_LONG).show();
            }
        });

    }
}
