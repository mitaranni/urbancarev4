package com.kominfo.lenovo.ncc_smartcity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import com.kominfo.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.kominfo.lenovo.ncc_smartcity.berita.MainBerita;
import com.kominfo.lenovo.ncc_smartcity.login.MainLogin;
import com.kominfo.lenovo.ncc_smartcity.lowongan_pekerjaan.MainIdrekan;
import com.kominfo.lenovo.ncc_smartcity.main_feature.Darurat;
import com.kominfo.lenovo.ncc_smartcity.main_feature.Pariwisata;
import com.kominfo.lenovo.ncc_smartcity.main_feature.Event;
import com.kominfo.lenovo.ncc_smartcity.main_feature.Layanan;
import com.kominfo.lenovo.ncc_smartcity.main_feature.Pasar_New_Layout;
import com.kominfo.lenovo.ncc_smartcity.tanggap.Main_tanggap;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by USER on 02/05/2018.
 */

public class MainAct2 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener {

    Intent i;

    String TAG = "MainAct2";

    ArrayList dataJadwalSholat;
    ArrayList dataCuaca;

    LinkedHashMap<String, Integer> fileMaps;
    Typeface face;

    private SliderLayout sliderLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        sliderLayout = findViewById(R.id.sl_sliderLayout);
        TextView tv = findViewById(R.id.tv_rollingTextJadwalSholat);
        tv.setSelected(true);
        fileMaps = new LinkedHashMap<>();

        ButterKnife.bind(this);

        getJadwalSholat();

        imageSlider();

        getCuacaOpenWeather();

    }

    //region get weather report from OpenWeather.org
    private void getCuacaOpenWeather() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URLCollection.DATA_SOURCE_OPEN_WEATHER; //copy url disini APInya

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    dataCuaca = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("weather");
                    JSONObject jsonObject_result = jsonArray.getJSONObject(0);

                    String getWeather = jsonObject_result.getString("description");
                    String getIcon = "ic_weather_" + jsonObject_result.getString("icon");
                    Log.d("GetCuacaOW", "getIcon : " + getIcon);

                    String getTemperature = jsonObject.getJSONObject("main").getString("temp") + "C";

                    TextView tv_weather = findViewById(R.id.tv_cuaca);
                    TextView tv_temperature = findViewById(R.id.tv_cuaca_temperature_current);
                    ImageView iv_weatherIcon = findViewById(R.id.iv_weatherIcon);

                    face = Typeface.createFromAsset(getAssets(), "font/myriad_pro_regular.otf");

                    tv_weather.setTypeface(face);
                    tv_temperature.setTypeface(face);


                    int iconID = getResources().getIdentifier(getIcon, "drawable", getPackageName());
                    Log.d("GetCuacaOW", "iconID : " + iconID);

                    iv_weatherIcon.setImageResource(iconID);

                    tv_weather.setText(getWeather);
                    tv_temperature.setText(getTemperature);

                } catch (JSONException e) {
                    Toast.makeText(MainAct2.this, "Error, something went wrong!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("apa", error.toString());
            }
        });

        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
    //endregion

    //region Image Slider
    private void imageSlider() {

        populateSlider();

        for (String name : fileMaps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);

            textSliderView
                    .description(name)
                    .image(fileMaps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(this);

//            textSliderView.bundle(new Bundle());
//            textSliderView.getBundle().putString("extra", name);

            sliderLayout.addSlider(textSliderView);
        }

        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(4000);
        sliderLayout.addOnPageChangeListener(this);
    }

    private void populateSlider() {
        fileMaps.clear();
        fileMaps.put("Tugu", R.drawable.slider_museum_purwa);
        fileMaps.put("Stasiun Malang", R.drawable.slider_masjid_agung_jami);
    }

    @Override
    protected void onStop() {
        sliderLayout.stopAutoCycle();
        super.onStop();
    }


    //region Obligatory Function
    @Override
    public void onSliderClick(BaseSliderView slider) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
    //endregion

    //endregion

    //region Jadwal Sholat
    private void getJadwalSholat() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URLCollection.DATA_SOURCE_JADWAL_SHOLAT_PKPU; //copy url disini APInya

        final TextView tv_rollingText = findViewById(R.id.tv_rollingTextJadwalSholat);
        face = Typeface.createFromAsset(getAssets(), "font/myriad_pro_regular.otf");

        try {
            tv_rollingText.setTypeface(face);
            Log.e("su", "onResponse: run");
        } catch (Exception e) {
            Log.e("su", "onResponse: " + e.getMessage());
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    dataJadwalSholat = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(response);
                    String date = "Jadwal Waktu Sholat Untuk Tanggal " + jsonObject.getString("tanggal");

                    final String subuh = "Subuh : " + jsonObject.getString("shubuh") + "  ";
                    final String dzuhur = "Dzuhur : " + jsonObject.getString("dzuhur") + "  ";
                    final String ashar = "Ashar : " + jsonObject.getString("ashr") + "  ";
                    final String maghrib = "Maghrib : " + jsonObject.getString("maghrib") + "  ";
                    final String isya = "Isya :" + jsonObject.getString("isya") + "  ";

                    String sumber = "Sumber : " + jsonObject.getString("sumber");

                    String rollingText = subuh + dzuhur + ashar + maghrib + isya;
                    tv_rollingText.setText(rollingText);

                } catch (JSONException e) {
                    Toast.makeText(MainAct2.this, "Error, something went wrong!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("apa", error.toString());
            }
        });

        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
    //endregion

    @OnClick({R.id.riv_layanan, R.id.riv_berita, R.id.riv_event, R.id.riv_etravel,
            R.id.riv_darurat, R.id.riv_harga_pasar,
            R.id.riv_tanggap_main, R.id.riv_loker, R.id.img_profil})
    void onMenuClicked(RoundedImageView roundedImageView) {
        switch (roundedImageView.getId()) {
            case R.id.riv_layanan:
                i = new Intent(this, Layanan.class);
                startActivity(i);
                break;
            case R.id.riv_berita:
                i = new Intent(this, MainBerita.class);
                startActivity(i);
                break;
            case R.id.riv_event:
                i = new Intent(this, Event.class);
                startActivity(i);
                break;
            case R.id.riv_etravel:
                i = new Intent(this, Pariwisata.class);
                startActivity(i);
                break;
//            case R.id.riv_kumpulan_layanan:
//                i = new Intent(this, KumpulanLayanan.class);
//                startActivity(i);
//                break;
            case R.id.riv_darurat:
                i = new Intent(this, Darurat.class);
                startActivity(i);
                break;
            case R.id.riv_harga_pasar:
                i = new Intent(this, Pasar_New_Layout.class);
                startActivity(i);
                break;
            case R.id.riv_tanggap_main:
                i = new Intent(this, Main_tanggap.class);
                startActivity(i);
                break;
            case R.id.riv_loker:
                i = new Intent(this, MainIdrekan.class);
                startActivity(i);
                break;
//            case R.id.riv_pasar_fragment:
//                i = new Intent(this, Pasar_New_Layout.class);
//                startActivity(i);
//                break;
            case R.id.img_profil:
                i = new Intent(this, MainLogin.class);
                startActivity(i);
                break;
        }
    }


    //navigation drawer
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dash) {
            // Handle the camera action
        } else if (id == R.id.nav_ch_email) {

        } else if (id == R.id.nav_ch_pass) {

        } else if (id == R.id.nav_notofikasi) {

//        } else if (id == R.id.nav_) {

        } else if (id == R.id.nav_log_in) {

        } else if (id == R.id.nav_log_in) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    //end navigation drawer
}

