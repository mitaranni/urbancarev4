package com.kominfo.lenovo.ncc_smartcity.tanggap.tanggap_model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by USER on 15/03/2018.
 */

public class List_Post {
    @SerializedName("id_post")
    private String id_post;
    @SerializedName("id_user")
    private String id_user;
    @SerializedName("event_photo_url")
    private String event_photo_url;
    @SerializedName("username")
    private String username;
    @SerializedName("profile_pic_url")
    private String profile_pic_url;
    @SerializedName("event_location")
    private String event_location;
    @SerializedName("event_status")
    private String event_status;
    @SerializedName("event_category")
    private String event_category;
    @SerializedName("event_description")
    private String event_description;
    @SerializedName("event_tgl")
    private String event_tgl;
    @SerializedName("event_time")
    private String event_time;

    public List_Post(String id_post, String id_user, String event_photo_url, String username, String profile_pic_url, String event_location, String event_status, String event_category, String event_description, String event_tgl, String event_time) {
        this.id_post = id_post;
        this.id_user = id_user;
        this.event_photo_url = event_photo_url;
        this.username = username;
        this.profile_pic_url = profile_pic_url;
        this.event_location = event_location;
        this.event_status = event_status;
        this.event_category = event_category;
        this.event_description = event_description;
        this.event_tgl = event_tgl;
        this.event_time = event_time;
    }

    public String getId_post() {
        return id_post;
    }

    public void setId_post(String id_post) {
        this.id_post = id_post;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getEvent_photo_url() {
        return event_photo_url;
    }

    public void setEvent_photo_url(String event_photo_url) {
        this.event_photo_url = event_photo_url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfile_pic_url() {
        return profile_pic_url;
    }

    public void setProfile_pic_url(String profile_pic_url) {
        this.profile_pic_url = profile_pic_url;
    }

    public String getEvent_location() {
        return event_location;
    }

    public void setEvent_location(String event_location) {
        this.event_location = event_location;
    }

    public String getEvent_status() {
        return event_status;
    }

    public void setEvent_status(String event_status) {
        this.event_status = event_status;
    }

    public String getEvent_category() {
        return event_category;
    }

    public void setEvent_category(String event_category) {
        this.event_category = event_category;
    }

    public String getEvent_description() {
        return event_description;
    }

    public void setEvent_description(String event_description) {
        this.event_description = event_description;
    }

    public String getEvent_tgl() {
        return event_tgl;
    }

    public void setEvent_tgl(String event_tgl) {
        this.event_tgl = event_tgl;
    }

    public String getEvent_time() {
        return event_time;
    }

    public void setEvent_time(String event_time) {
        this.event_time = event_time;
    }
}
