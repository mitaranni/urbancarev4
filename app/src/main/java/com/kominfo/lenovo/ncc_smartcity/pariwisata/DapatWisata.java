package com.kominfo.lenovo.ncc_smartcity.pariwisata;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kominfo.lenovo.ncc_smartcity.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DapatWisata {

    public DapatWisata() {

    }

    public void DapatSemuaWisata(String uri, final Activity context, final String selectedFeature) {
        final ArrayList<String> titles = new ArrayList<>();
        final ArrayList<String> alamat = new ArrayList<>();
        final ArrayList<String> deskripsi = new ArrayList<>();
        final ArrayList<String> telepon = new ArrayList<>();
        final ArrayList<String> gambar = new ArrayList<>();
        titles.clear();
        alamat.clear();
        deskripsi.clear();
        telepon.clear();
        gambar.clear();
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = uri;

        final ProgressDialog progressDialog = new ProgressDialog(context) {
            @Override
            public void onBackPressed() {
                context.finish();
            }
        };
        //picolo
        progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {


                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
//                                Log.e("apa", response);
                        progressDialog.dismiss();

                        try {


                            JSONArray panda = new JSONArray(response);
                            for (int i = 0; i < panda.length(); i++) {
                                JSONObject panda1 = panda.getJSONObject(i);
                                titles.add(panda1.getString("nama_tempat"));
                                alamat.add(panda1.getString("alamat_tempat"));
                                deskripsi.add(panda1.getString("deskripsi_tempat"));
                                telepon.add(panda1.getString("telepon"));
                                gambar.add(panda1.getString("gambar"));
                            }
                            ListView listView = context.findViewById(R.id.lv_layanan_list);

                            listView.setAdapter(new CustomListViewAdapter2(context, titles, alamat, deskripsi, telepon, gambar, selectedFeature));

                        } catch (JSONException e) {
                            Log.e("panda", e.toString());
                            Toast.makeText(context, "Error, something went wrong!", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
//                mTextView.setText("That didn't work!");
                Log.e("apa", error.toString());
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }
}
