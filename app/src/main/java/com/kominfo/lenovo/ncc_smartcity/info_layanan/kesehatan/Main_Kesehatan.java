package com.kominfo.lenovo.ncc_smartcity.info_layanan.kesehatan;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kominfo.lenovo.ncc_smartcity._globalVariable.StaticVariable;
import com.kominfo.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.kominfo.lenovo.ncc_smartcity.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arimahardika on 06/03/2018.
 */

public class Main_Kesehatan extends Activity {

    String DATA_SOURCE_URL;
    RecyclerView recyclerView;

    Adapter_Kesehatan adapter_kesehatan;
    List<Model_Kesehatan> listKesehatan;

    ProgressDialog progressDialog;
    String loadingMessage = "Loading";

    LinearLayout ll_sumber_kesehatan;
    String coordinate;

    String selectedFeature;

    TextView textView;
    Typeface customFont;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kesehatan_main);

        customFont = Typeface.createFromAsset(getAssets(), "font/MontserratBold.ttf");
        textView = findViewById(R.id.textViewkesehatan);
        textView.setTypeface(customFont);

        Log.d("KESEHATAN", "onCreate: im inside main kesehatan");

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            selectedFeature = (String) bundle.get(StaticVariable.NAMA_LAYANAN);
        }

        selectDataURL(selectedFeature);

        ll_sumber_kesehatan = findViewById(R.id.ll_sumber_kesehatan);
        recyclerView = findViewById(R.id.kesehatan_recyclerView);
        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        listKesehatan = new ArrayList<>();

        grabDataKesehatan();
    }

    private void selectDataURL(String category) {
        switch (category) {
            case StaticVariable.RUMAH_SAKIT_UMUM:
                DATA_SOURCE_URL = URLCollection.DATA_SOURCE_KESEHATAN_RUMAH_SAKIT;
                break;
            case StaticVariable.RUMAH_SAKIT_IBU_DAN_ANAK:
                DATA_SOURCE_URL = URLCollection.DATA_SOURCE_KESEHATAN_RUMAH_SAKIT_IBU_ANAK;
                break;
            case StaticVariable.RUMAH_SAKIT_BERSALIN:
                DATA_SOURCE_URL = URLCollection.DATA_SOURCE_KESEHATAN_RUMAH_SAKIT_BERSALIN;
                break;
            case StaticVariable.PUSKESMAS:
                DATA_SOURCE_URL = URLCollection.DATA_SOURCE_KESEHATAN_PUSKESMAS;
                break;
            case StaticVariable.PUSKESMAS_UPT:
                DATA_SOURCE_URL = URLCollection.DATA_SOURCE_KESEHATAN_PUSKESMAS_UPT_PEMBANTU;
                break;
            case StaticVariable.KLINIK:
                DATA_SOURCE_URL = URLCollection.DATA_SOURCE_KESEHATAN_KLINIK;
                break;
            case StaticVariable.APOTEK:
                DATA_SOURCE_URL = URLCollection.DATA_SOURCE_KESEHATAN_APOTEK;
                break;
            case StaticVariable.DOKTER_GIGI:
                DATA_SOURCE_URL = URLCollection.DATA_SOURCE_KESEHATAN_DOKTER_GIGI;
                break;
            case StaticVariable.DOKTER_UMUM:
                DATA_SOURCE_URL = URLCollection.DATA_SOURCE_KESEHATAN_DOKTER_UMUM;
                break;
            case StaticVariable.LABORATORIUM:
                DATA_SOURCE_URL = URLCollection.DATA_SOURCE_KESEHATAN_LABORATORIUM;
                break;
            case StaticVariable.OPTIK:
                DATA_SOURCE_URL = URLCollection.DATA_SOURCE_KESEHATAN_OPTIK;
                break;
        }
    }

    @Override
    protected void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        super.onDestroy();
    }

    private void grabDataKesehatan() {
        progressDialog = new ProgressDialog(this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };

        progressDialog.setMessage(loadingMessage);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, DATA_SOURCE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                Model_Kesehatan model_kesehatan = new Model_Kesehatan(
                                        jsonObject.getString("tipe"),
                                        jsonObject.getString("nama"),
                                        jsonObject.getString("jalan"),
                                        jsonObject.getString("telp")
                                );

                                listKesehatan.add(model_kesehatan);
                            }
                            adapter_kesehatan = new Adapter_Kesehatan(listKesehatan, getApplicationContext());
                            recyclerView.setAdapter(adapter_kesehatan);

                            ll_sumber_kesehatan.setVisibility(View.VISIBLE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Main_Kesehatan.this, error.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("VolleyError", error.toString());
                    }
                }
        );

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
