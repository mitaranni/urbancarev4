package com.kominfo.lenovo.ncc_smartcity.pariwisata;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.kominfo.lenovo.ncc_smartcity.R;
import com.kominfo.lenovo.ncc_smartcity._globalVariable.StaticVariable;

/**
 * Created by Dovie Yudhawiratama on 2/25/2018.
 */


public class InfoWisata extends Activity implements View.OnClickListener {

    CardView sejarah, kampung, edukasi, rekreasi, belanja, religi, olahraga, hutan;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pariwisata_sub_wisata);

        sejarah = findViewById(R.id.sejarah);
        kampung = findViewById(R.id.kampung);
        edukasi = findViewById(R.id.edukasi);
        rekreasi = findViewById(R.id.rekreasi);
        belanja = findViewById(R.id.belanja);
        religi = findViewById(R.id.religi);
        olahraga = findViewById(R.id.olahraga);
        hutan = findViewById(R.id.hutan);

        sejarah.setOnClickListener(this);
        kampung.setOnClickListener(this);
        edukasi.setOnClickListener(this);
        rekreasi.setOnClickListener(this);
        belanja.setOnClickListener(this);
        religi.setOnClickListener(this);
        olahraga.setOnClickListener(this);
        hutan.setOnClickListener(this);


    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.belanja:
                i = new Intent(this, InfoWisataSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_BELANJA);
                this.startActivity(i);
                break;
            case R.id.edukasi:
                i = new Intent(this, InfoWisataSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_EDUKASI);
                this.startActivity(i);
                break;
            case R.id.sejarah:
                i = new Intent(this, InfoWisataSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_SEJARAH);
                this.startActivity(i);
                break;
            case R.id.religi:
                i = new Intent(this, InfoWisataSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_RELIGI);
                this.startActivity(i);
                break;


            case R.id.rekreasi:
                i = new Intent(this, InfoWisataSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_REKREASI);
                this.startActivity(i);
                break;
            case R.id.kampung:
                i = new Intent(this, InfoWisataSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_KAMPUNG);
                this.startActivity(i);
                break;
            case R.id.olahraga:
                i = new Intent(this, InfoWisataSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_OLAHRAGA);
                this.startActivity(i);
                break;
            case R.id.hutan:
                i = new Intent(this, InfoWisataSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_TAMAN_HUTAN);
                this.startActivity(i);
                break;
        }
    }
}
