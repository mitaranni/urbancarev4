package com.kominfo.lenovo.ncc_smartcity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import com.kominfo.lenovo.ncc_smartcity._globalVariable.MessageVariabe;
import com.kominfo.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.kominfo.lenovo.ncc_smartcity.berita.MainBerita;
import com.kominfo.lenovo.ncc_smartcity.cek_tagihan.CekTagihan;
import com.kominfo.lenovo.ncc_smartcity.dashAkun.ChEmail;
import com.kominfo.lenovo.ncc_smartcity.dashAkun.ChPass;
import com.kominfo.lenovo.ncc_smartcity.dashAkun.MainAkun;
import com.kominfo.lenovo.ncc_smartcity.dashAkun.VerifikasiKTP;
import com.kominfo.lenovo.ncc_smartcity.event.Event_New_Layout;
import com.kominfo.lenovo.ncc_smartcity.internalLib.CheckConn;
import com.kominfo.lenovo.ncc_smartcity.internalLib.RetrofitLib.base_url.Base_url;
import com.kominfo.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management.SendDataLogin;
import com.kominfo.lenovo.ncc_smartcity.internalLib.Session;
import com.kominfo.lenovo.ncc_smartcity.internalLib.SessionCheck;
import com.kominfo.lenovo.ncc_smartcity.internalLib.TextSafety;
import com.kominfo.lenovo.ncc_smartcity.login.MainLogin;
import com.kominfo.lenovo.ncc_smartcity.lowongan_pekerjaan.MainIdrekan;
import com.kominfo.lenovo.ncc_smartcity.main_feature.Darurat;
import com.kominfo.lenovo.ncc_smartcity.main_feature.Pariwisata;
import com.kominfo.lenovo.ncc_smartcity.main_feature.Layanan;
import com.kominfo.lenovo.ncc_smartcity.main_feature.Pasar_New_Layout;
import com.kominfo.lenovo.ncc_smartcity.register_new.MainRegister_new;
import com.kominfo.lenovo.ncc_smartcity.register_new.TakePhoto_new;
import com.kominfo.lenovo.ncc_smartcity.tanggap.Main_tanggap;
import com.kominfo.lenovo.ncc_smartcity.tutorial.Tutorial;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener {

    String BASE_URL = URLCollection.DATA_SOURCE_LOKER;
    Intent i;

    MenuItem iDash, iProfil, iNotif, iVertEmail, iVertKtp, iChPass, iChEmail,
            iLogin, iLogout, iForgetPass, iReg, iHelp;
    MenuItem iUserCrtl;
    // gLogin, gNoLogin;
    String TAG = "MainActivity";

    ArrayList dataJadwalSholat;
    ArrayList dataCuaca;

    LinkedHashMap<String, Integer> fileMaps;
    Typeface face;
    NavigationView navigationView;
    SessionCheck sessionCheck = new SessionCheck();
    TextView tvEmail, tvUsername;
    Session session = new Session();
    String[] data_session;
    CircleImageView img;
    String nik_ = "";
    //region Jadwal Sholat
    String date, subuh, dzuhur, ashar, maghrib, isya, sumber;
    Button btnSave, btnCancle;
    EditText etEmail;
    TextSafety textSafety = new TextSafety();
    CheckConn checkConn = new CheckConn();
    String TOKEN_MOBILE = "X00W";
    Call<ResponseBody> getdata;
    ;
    SendDataLogin base_url_management;
    ProgressDialog progressDialog;
    String user_id = null;
    de.hdodenhof.circleimageview.CircleImageView imgview;
    private SliderLayout sliderLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main2);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        iUserCrtl = navigationView.getMenu().findItem(R.id.usr_ctrl);
        iLogout = navigationView.getMenu().findItem(R.id.nav_log_out);
        iVertEmail = navigationView.getMenu().findItem(R.id.nav_vert_email);
        iVertKtp = navigationView.getMenu().findItem(R.id.nav_vert_ktp);

        iReg = navigationView.getMenu().findItem(R.id.nav_register);
        iLogin = navigationView.getMenu().findItem(R.id.nav_log_in);
        iForgetPass = navigationView.getMenu().findItem(R.id.nav_forget_pass);

        //img = (CircleImageView) navigationView.getHeaderView(0).findViewById(R.id.hdImgProfil);

        //header
        tvEmail = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tvEmail);
        tvUsername = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tvUsername);
        imgview = (de.hdodenhof.circleimageview.CircleImageView) navigationView.getHeaderView(0).findViewById(R.id.hdImgProfil);

//        data_session = new SessionCheck().seesionLoginChecker(MainActivity.this);
//        Log.e(TAG, "onClick: " + data_session);
//        if (data_session != null && Boolean.valueOf(data_session[0]) == true) {
//            Log.e(TAG, "onClick: data_session[0]" + data_session[0]);
//
//            iUserCrtl.setVisible(true);
//            iLogout.setVisible(true);
//
//            iReg.setVisible(false);
//            iLogin.setVisible(false);
//            iForgetPass.setVisible(false);
//
//            tvUsername.setVisibility(View.VISIBLE);
//            tvEmail.setVisibility(View.VISIBLE);
//
//            Picasso.with(MainActivity.this)
//                    .load(BASE_URL + data_session[3])
//                    .error(R.mipmap.ic_anonymous)
//                    .into(img);
//            tvUsername.setText(data_session[2]);
////            Log.e(TAG, "onCreate: username: "+data_session[2]);
////            Log.e(TAG, "onCreate: email: "+data_session[5]);
//            tvEmail.setText(data_session[5]);
//            nik_ = data_session[4];
//        }


        sliderLayout = findViewById(R.id.sl_sliderLayout);

        TextView tv = findViewById(R.id.tv_rollingTextJadwalSholat);
        tv.setSelected(true);

        fileMaps = new LinkedHashMap<>();

        ButterKnife.bind(this);

        getJadwalSholat();

        imageSlider();

        getCuacaOpenWeather();
    }

    @Override
    public void onStart() {
        super.onStart();

        data_session = new SessionCheck().seesionLoginChecker(MainActivity.this);
        Log.e(TAG, "onClick: " + data_session);
        if (data_session != null && Boolean.valueOf(data_session[0]) == true) {
            Log.e(TAG, "onClick: data_session[0]" + data_session[0]);

            iUserCrtl.setVisible(true);
            iLogout.setVisible(true);

            iReg.setVisible(false);
            iLogin.setVisible(false);
            iForgetPass.setVisible(false);

            tvUsername.setVisibility(View.VISIBLE);
            tvEmail.setVisibility(View.VISIBLE);

            int status_register = Integer.valueOf(data_session[7]);
            int ty_warga = Integer.valueOf(data_session[8]);
            if (ty_warga == 0) {
                if (status_register == 0) {
                    iVertKtp.setVisible(false);
                    iVertEmail.setVisible(true);
                } else if (status_register == 1) {
                    iVertKtp.setVisible(false);
                    iVertEmail.setVisible(false);
                }
            } else {
                if (status_register == 0) {
                    iVertKtp.setVisible(true);
                    iVertEmail.setVisible(true);
                } else if (status_register == 2) {
                    iVertKtp.setVisible(true);
                    iVertEmail.setVisible(false);
                } else if (status_register == 1) {
                    iVertKtp.setVisible(false);
                    iVertEmail.setVisible(false);
                }

            }

//            Picasso.with(MainActivity.this)
//                    .load(BASE_URL + data_session[3])
//                    .error(R.mipmap.ic_anonymous)
//                    .into(img);
            tvUsername.setText(data_session[2]);
//            Log.e(TAG, "onCreate: username: "+data_session[2]);
//            Log.e(TAG, "onCreate: email: "+data_session[5]);
            Log.e(TAG, "onStart: " + new URLCollection().BASE_URL_FOTO + data_session[3]);
            Picasso.with(this).load(new URLCollection().BASE_URL_FOTO + data_session[3]).into(imgview);
            //imgview =
            tvEmail.setText(data_session[5]);
            nik_ = data_session[4];
        }

    }
    //endregion

    //region get weather report from OpenWeather.org
    private void getCuacaOpenWeather() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URLCollection.DATA_SOURCE_OPEN_WEATHER; //copy url disini APInya

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    dataCuaca = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("weather");
                    JSONObject jsonObject_result = jsonArray.getJSONObject(0);

                    String getWeather = jsonObject_result.getString("description");
                    String getIcon = "ic_weather_" + jsonObject_result.getString("icon");
                    Log.d("GetCuacaOW", "getIcon : " + getIcon);

                    String getTemperature = jsonObject.getJSONObject("main").getString("temp") + "C";

                    TextView tv_weather = findViewById(R.id.tv_cuaca);
                    TextView tv_temperature = findViewById(R.id.tv_cuaca_temperature_current);
                    ImageView iv_weatherIcon = findViewById(R.id.iv_weatherIcon);

                    face = Typeface.createFromAsset(getAssets(), "font/myriad_pro_regular.otf");

                    tv_weather.setTypeface(face);
                    tv_temperature.setTypeface(face);


                    int iconID = getResources().getIdentifier(getIcon, "drawable", getPackageName());
                    Log.d("GetCuacaOW", "iconID : " + iconID);

                    iv_weatherIcon.setImageResource(iconID);

                    tv_weather.setText(getWeather);
                    tv_temperature.setText(getTemperature);

                } catch (JSONException e) {
                    Toast.makeText(MainActivity.this, "Error, something went wrong!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("apa", error.toString());
            }
        });

        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    //region Image Slider
    private void imageSlider() {

        populateSlider();

        for (String name : fileMaps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);

            textSliderView
                    .description(name)
                    .image(fileMaps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(this);

//            textSliderView.bundle(new Bundle());
//            textSliderView.getBundle().putString("extra", name);

            sliderLayout.addSlider(textSliderView);
        }

        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(4000);
        sliderLayout.addOnPageChangeListener(this);
    }

    private void populateSlider() {
        fileMaps.clear();
        fileMaps.put("Tugu Malang", R.drawable.slider_tugu);
        fileMaps.put("Stasiun Malang", R.drawable.slider_stasiun);
        fileMaps.put("Alun-Alun Malang", R.drawable.slider_alun_alun_malang);
        fileMaps.put("Kampung Topeng", R.drawable.slider_kampung_topeng);
        fileMaps.put("Masjid Agung Jami", R.drawable.slider_masjid_agung_jami);
        fileMaps.put("Museum Mpu Purwa", R.drawable.slider_museum_purwa);
        fileMaps.put("Kampung Warna-Warni Jodipan", R.drawable.slider_jodipan_malang);
    }

    @Override
    protected void onStop() {
        sliderLayout.stopAutoCycle();
        super.onStop();
    }

    //region Obligatory Function
    @Override
    public void onSliderClick(BaseSliderView slider) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
    }
    //endregion

    //endregion

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @OnClick({R.id.tv_rollingTextJadwalSholat})
    public void jadwalSholatPopUp() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.jadwal_sholat_dialog, null);
        dialogBuilder.setView(dialogView);
        TextView tanggalText = (TextView) dialogView.findViewById(R.id.tanggal);
        tanggalText.setText(date);
        TextView subuhText = (TextView) dialogView.findViewById(R.id.subuh);
        subuhText.setText(subuh);
        TextView dzuhurText = (TextView) dialogView.findViewById(R.id.dzuhur);
        dzuhurText.setText(dzuhur);
        TextView asharText = (TextView) dialogView.findViewById(R.id.ashar);
        asharText.setText(ashar);
        TextView maghribText = (TextView) dialogView.findViewById(R.id.maghrib);
        maghribText.setText(maghrib);
        TextView isyaText = (TextView) dialogView.findViewById(R.id.isya);
        isyaText.setText(isya);
        TextView sumberText = (TextView) dialogView.findViewById(R.id.sumber);
        sumberText.setText(sumber);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

    }

    private void getJadwalSholat() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URLCollection.DATA_SOURCE_JADWAL_SHOLAT_PKPU; //copy url disini APInya
        final TextView tv_rollingText = findViewById(R.id.tv_rollingTextJadwalSholat);

        face = Typeface.createFromAsset(getAssets(), "font/myriad_pro_regular.otf");

        try {
            tv_rollingText.setTypeface(face);
            Log.e("su", "onResponse: run");
        } catch (Exception e) {
            Log.e("su", "onResponse: " + e.getMessage());
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    dataJadwalSholat = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(response);
                    date = "Jadwal Waktu Sholat Untuk Tanggal " + jsonObject.getString("tanggal") + " ";

                    subuh = "Subuh : " + jsonObject.getString("shubuh") + "  ";
                    dzuhur = "Dzuhur : " + jsonObject.getString("dzuhur") + "  ";
                    ashar = "Ashar : " + jsonObject.getString("ashar") + "  ";
                    maghrib = "Maghrib : " + jsonObject.getString("maghrib") + "  ";
                    isya = "Isya :" + jsonObject.getString("isya") + "  ";

                    sumber = "Sumber : " + jsonObject.getString("sumber");

                    String rollingText = date + " " + subuh + dzuhur + ashar + maghrib + isya;
                    tv_rollingText.setText(rollingText);
                    date = jsonObject.getString("tanggal");

                    subuh = jsonObject.getString("shubuh");
                    dzuhur = jsonObject.getString("dzuhur");
                    ashar = jsonObject.getString("ashar");
                    maghrib = jsonObject.getString("maghrib");
                    isya = jsonObject.getString("isya");

                    sumber = jsonObject.getString("sumber");

                } catch (JSONException e) {
                    Log.e("errorjson", e.toString());
                    Toast.makeText(MainActivity.this, "Error, something went wrong!" + e.toString(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("apa", error.toString());
            }
        });

        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
    //endregion

    @OnClick({R.id.riv_layanan,
            R.id.riv_berita,
            R.id.riv_event,
            R.id.riv_etravel,
            R.id.riv_darurat,
            R.id.riv_harga_pasar,
            R.id.riv_tanggap_main,
            R.id.riv_loker,
            R.id.img_profil,
            R.id.riv_cekTagihan,
            R.id.riv_help_tutorial,
            R.id.riv_aboutUs})
    void onMenuClicked(RoundedImageView roundedImageView) {
        switch (roundedImageView.getId()) {
            case R.id.riv_layanan:
                i = new Intent(this, Layanan.class);
                startActivity(i);
                break;
            case R.id.riv_berita:
                i = new Intent(this, MainBerita.class);
                startActivity(i);
                break;
            case R.id.riv_event:
                i = new Intent(this, Event_New_Layout.class);
                startActivity(i);
                break;
            case R.id.riv_etravel:
                i = new Intent(this, Pariwisata.class);
                startActivity(i);
                break;
            case R.id.riv_darurat:
                i = new Intent(this, Darurat.class);
                startActivity(i);
                break;
            case R.id.riv_harga_pasar:
                i = new Intent(this, Pasar_New_Layout.class);
                startActivity(i);
                break;
            case R.id.riv_tanggap_main:
                i = new Intent(this, Main_tanggap.class);
                startActivity(i);
                break;
            case R.id.riv_loker:
                i = new Intent(this, MainIdrekan.class);
                startActivity(i);
                break;
            case R.id.img_profil:
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.openDrawer(GravityCompat.START);
//                PopupMenu popupMenu = new PopupMenu(MainActivity.this, roundedImageView);
//                popupMenu.getMenuInflater().inflate(R.menu.home_options, popupMenu.getMenu());
//
//                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//                        switch (item.getTitle().toString()) {
//                            case "Login":
//                                i = new Intent(MainActivity.this, Profile_Main.class);
//                                startActivity(i);
//                                break;
//                        }
//                        return true;
//                    }
//                });
//                popupMenu.show();
                break;
            case R.id.riv_cekTagihan:
                i = new Intent(this, CekTagihan.class);
                startActivity(i);
                break;
            case R.id.riv_help_tutorial:
                i = new Intent(this, Tutorial.class);
                startActivity(i);
                break;
            case R.id.riv_aboutUs:
                i = new Intent(this, AboutUs.class);
                startActivity(i);
                break;
        }
    }

    //navigation drawer
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dash) {
            //dashboard ***
            i = new Intent(MainActivity.this, MainAkun.class);
            i.putExtra("user_id", data_session[1]);
            startActivity(i);

        } else if (id == R.id.nav_prof) {
            i = new Intent(MainActivity.this, MainAkun.class);
            startActivity(i);

        } else if (id == R.id.nav_notofikasi) {
            //notifikasi ***
            i = new Intent(MainActivity.this, MainAkun.class);
            i.putExtra("user_id", data_session[1]);
            startActivity(i);

        } else if (id == R.id.nav_vert_ktp) {
            i = new Intent(MainActivity.this, VerifikasiKTP.class);
            i.putExtra("user_id", data_session[1]);
            startActivity(i);

        } else if (id == R.id.nav_vert_email) {
            //Email Vert ***
            if (checkConn.isConnected(MainActivity.this)) {
                Log.e(TAG, "onNavigationItemSelected: connect");
                setData();
            }
//            i = new Intent(MainActivity.this, MainAkun.class);
//            startActivity(i);

        } else if (id == R.id.nav_ch_email) { //finish
            i = new Intent(MainActivity.this, ChEmail.class);
            i.putExtra("user_id", data_session[1]);
            startActivity(i);

        } else if (id == R.id.nav_ch_pass) {
            i = new Intent(MainActivity.this, ChPass.class);
            i.putExtra("user_id", data_session[1]);
            startActivity(i);
        } else if (id == R.id.nav_log_in) { //finish
            i = new Intent(MainActivity.this, MainLogin.class);
            startActivity(i);
        } else if (id == R.id.nav_log_out) { //finish
            session.sessionDestroy(MainActivity.this);
            i = new Intent(MainActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_forget_pass) {
            //forget pass ***
            i = new Intent(MainActivity.this, MainAkun.class);
            startActivity(i);
        } else if (id == R.id.nav_register) { //finish
            i = new Intent(MainActivity.this, MainRegister_new.class);
            startActivity(i);
        } else if (id == R.id.nav_ch_img_profil) {
            i = new Intent(MainActivity.this, TakePhoto_new.class);
            i.putExtra("nik", nik_);
            i.putExtra("ty_take_img", "1");
            startActivity(i);
            //finish();
        } else if (id == R.id.nav_bantuan) {
            i = new Intent(MainActivity.this, Tutorial.class);
            startActivity(i);
        } else if (id == R.id.nav_tentang) {
            i = new Intent(MainActivity.this, AboutUs.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    //end navigation drawer

    private void setData() {
        progressDialog = new ProgressDialog(MainActivity.this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };

        progressDialog.setMessage(MessageVariabe.MESSAGE_LOADING);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        base_url_management = Base_url.getClient_notGson(BASE_URL)
                .create(SendDataLogin.class);
        getdata = base_url_management.UpdateEmail(data_session[1]
                , data_session[5]
                , TOKEN_MOBILE
                , "1");

        setData(getdata);
    }

    public void setData(Call<ResponseBody> getdata) {
        Log.e(TAG, "adapterRequest: run");
        getdata.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                progressDialog.dismiss();
                JSONObject dataJson = null;
                String message_out = null;
                try {
                    try {
                        dataJson = new JSONObject(response.body().string());
                        Log.e(TAG, "onResponse: " + dataJson);
                        String status = dataJson.getJSONObject("body_message").getString("status");
                        message_out = dataJson.getJSONObject("body_message").getString("message");
                        if (Boolean.valueOf(status)) {
                            //finish();
                        }
                        Toast.makeText(MainActivity.this, message_out, Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        Log.e(TAG, "onResponse: " + e.getMessage());
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "onResponse: dataJson: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, new MessageVariabe().MESSAGE_ERROR_RESPONSE_FAIL_SERVER, Toast.LENGTH_LONG).show();
            }
        });

    }
}

