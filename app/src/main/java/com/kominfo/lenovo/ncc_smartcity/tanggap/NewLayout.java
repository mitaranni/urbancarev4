package com.kominfo.lenovo.ncc_smartcity.tanggap;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.kominfo.lenovo.ncc_smartcity.R;

/**
 * Created by arimahardika on 09/04/2018.
 */

public class NewLayout extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tanggap_cardview_new);
    }
}
