package com.kominfo.lenovo.ncc_smartcity.event;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by USER on 30/05/2018.
 */

public interface ApiService {

    /*
    Retrofit get annotation with our URL
    And our method that will return us the List of Contacts
    */
    @Multipart
    @POST("postevent")
    Call<Result> uploadEvent(@Part MultipartBody.Part file);
}