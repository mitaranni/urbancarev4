package com.kominfo.lenovo.ncc_smartcity.main_feature;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.kominfo.lenovo.ncc_smartcity.R;

/**
 * Created by lenovo on 2/5/2018.
 */

public class Darurat extends Activity {

    CardView call_polisi, call_ambulan, call_psc, call_damkar, call_pdam, call_pln;
    String NO_POLISI = "110";
    String NO_AMBULAN = "118";
    String NO_PSC = "119";
    String NO_DAMKAR = "0341364617";
    String NO_PDAM = "0341716900";
    String NO_PLN = "123";

    Typeface customFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_darurat);
        customFont = Typeface.createFromAsset(getAssets(), "font/myriad_pro_regular.otf");
        TextView textView = findViewById(R.id.telepondarurat_tittle);
        textView.setTypeface(customFont);
        call_polisi = findViewById(R.id.call_polisi);
        call_ambulan = findViewById(R.id.call_ambulan);
        call_psc = findViewById(R.id.call_psc);
        call_damkar = findViewById(R.id.call_damkar);
        call_pdam = findViewById(R.id.call_pdam);
        call_pln = findViewById(R.id.call_pln);


        callAction();
    }

    private void callAction() {
        call_polisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call = new Intent(Intent.ACTION_DIAL);
                call.setData(Uri.parse("tel:" + NO_POLISI));
                startActivity(call);
            }
        });

        call_ambulan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call = new Intent(Intent.ACTION_DIAL);
                call.setData(Uri.parse("tel:" + NO_AMBULAN));
                startActivity(call);
            }
        });
        call_psc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call = new Intent(Intent.ACTION_DIAL);
                call.setData(Uri.parse("tel:" + NO_PSC));
                startActivity(call);
            }
        });

        call_damkar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call = new Intent(Intent.ACTION_DIAL);
                call.setData(Uri.parse("tel:" + NO_DAMKAR));
                startActivity(call);
            }
        });
        call_pdam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call = new Intent(Intent.ACTION_DIAL);
                call.setData(Uri.parse("tel:" + NO_PDAM));
                startActivity(call);
            }
        });

        call_pln.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call = new Intent(Intent.ACTION_DIAL);
                call.setData(Uri.parse("tel:" + NO_PLN));
                startActivity(call);
            }
        });


    }
}
