package com.kominfo.lenovo.ncc_smartcity.sekolah;

public class Model {

    public String no;
    public String npsn;
    public String nama;
    public String alamat;

    public Model() {

    }

    public Model(String no, String npsn,String nama, String alamat) {
        this.no = no;
        this.npsn = npsn;
        this.nama = nama;
        this.alamat = alamat;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getNpsn() {
        return npsn;
    }

    public void setNpsn(String npsn) {
        this.npsn = npsn;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

}
