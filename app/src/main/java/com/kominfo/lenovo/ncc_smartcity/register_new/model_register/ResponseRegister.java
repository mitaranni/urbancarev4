package com.kominfo.lenovo.ncc_smartcity.register_new.model_register;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by USER on 22/03/2018.
 */

public class ResponseRegister {
    @SerializedName("id_response")
    String id_status;

    @SerializedName("body")
    List<List_Response> response_body;

    public ResponseRegister(String id_status, List<List_Response> response_body) {
        this.id_status = id_status;
        this.response_body = response_body;
    }

    public String getId_status() {
        return id_status;
    }

    public void setId_status(String id_status) {
        this.id_status = id_status;
    }

    public List<List_Response> getResponse_body() {
        return response_body;
    }

    public void setResponse_body(List<List_Response> response_body) {
        this.response_body = response_body;
    }
}
