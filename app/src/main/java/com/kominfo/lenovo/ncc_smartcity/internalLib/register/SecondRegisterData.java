package com.kominfo.lenovo.ncc_smartcity.internalLib.register;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by USER on 02/04/2018.
 */

public class SecondRegisterData {

    String TAG = "SecondRegisterData";

    public String[] SecondRegisterGet(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("secReg", Context.MODE_PRIVATE);
        Log.e(TAG, "seesionLoginChecker: " + prefs);
        if (prefs != null) {
            if (prefs.getBoolean("status_akses", false)) {
                String nama = prefs.getString("nama", null);
                String alamat = prefs.getString("alamat", null);
                String tmp_lhr = prefs.getString("tmp_lhr", null);
                String tgl_lhr = prefs.getString("tgl_lhr", null);//"No name defined" is the default value.
                long jk = prefs.getLong("jk", 1); //0 is the default value.
                long wn = prefs.getLong("wn", 1); //0 is the default value.

                Log.e(TAG, "seesionLoginChecker: " + nama);
                String[] session_val = {nama, alamat, tmp_lhr, tgl_lhr, String.valueOf(jk), String.valueOf(wn)};
                return session_val;
            }
        }

        return null;
    }

    public void SecondRegisterSet(Context context,
                                  String nama, String alamat, String tmp_lhr, String tgl_lhr,
                                  long jk, long wn) {
        Log.e(TAG, "loginSession: run");
        try {
            Log.e(TAG, "loginSession: run");
            SharedPreferences sharedPref = context.getSharedPreferences("secReg", Context.MODE_PRIVATE);
            Log.e(TAG, "loginSession: sharedPref: " + sharedPref);
            SharedPreferences.Editor editor = sharedPref.edit();
            Log.e(TAG, "loginSession: editor: " + editor);

            editor.putString("nama", nama);
            editor.putString("alamat", alamat);
            editor.putString("tmp_lhr", tmp_lhr);
            editor.putString("tgl_lhr", tgl_lhr);

            editor.putLong("jk", jk);
            editor.putLong("wn", wn);

            editor.putBoolean("status_akses", true);
            editor.apply();
        } catch (Exception e) {
            Log.e(TAG, "loginSession: " + e.getMessage());
        }
    }

    public void SecondRegisterDestroy(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("secReg", Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
        Log.e(TAG, "sessionDestroy: session destroy");
    }
}
