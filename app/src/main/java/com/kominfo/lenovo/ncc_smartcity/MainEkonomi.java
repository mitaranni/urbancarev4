package com.kominfo.lenovo.ncc_smartcity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.kominfo.lenovo.ncc_smartcity.main_feature.Pasar_New_Layout;

public class MainEkonomi extends Activity implements View.OnClickListener{

    CardView e_ikm, e_perdagangan, e_ukm, e_pasar ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_ekonomi);

        e_pasar = (CardView) findViewById(R.id.e_pasar);

        e_pasar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent i ;

        switch (view.getId()){
            case R.id.e_pasar : i = new Intent(this, Pasar_New_Layout.class);startActivity(i); break;

            default: break;
        }

    }
}
