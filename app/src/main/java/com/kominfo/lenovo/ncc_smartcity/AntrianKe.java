package com.kominfo.lenovo.ncc_smartcity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

public class AntrianKe extends Activity implements View.OnClickListener{

    CardView a_puskesmas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.antrianpuskesmas);

        a_puskesmas = (CardView) findViewById(R.id.a_puskesmas);

        a_puskesmas.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i ;

        switch (v.getId()){
            case R.id.a_puskesmas : i = new Intent(this, AntrianP.class);startActivity(i); break;

            default: break;
        }
    }

}
