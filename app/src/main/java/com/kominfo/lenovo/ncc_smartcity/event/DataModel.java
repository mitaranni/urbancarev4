package com.kominfo.lenovo.ncc_smartcity.event;

/**
 * Created by cebum on 19/02/18.
 */

public class DataModel {
    //    img": "http://eventmalang.net/wp-content/uploads/2018/02/MP-Sharing-Session-Online-Danusku.id-Copy-150x150.jpg",
//            "link": "http://eventmalang.net/event/sharing-session-online-business-talk-with-patria-prima-putra/",
//            "title": "Sharing Session Online “Business Talk with Patria Prima Putra”",
//            "description": "Sharing session online “business talk with patria prima putra” tanggal : minggu, 25 februari 2018 tempat : online (group line khusus)…",
//            "category": "Cat
    String img;
    String link;
    String title;
    String description;
    String category;

    public DataModel(String img, String link, String title, String description, String category) {
        this.img = img;
        this.link = link;
        this.title = title;
        this.description = description;
        this.category = category;

    }

    public String getImg() {
        return img;
    }

    public String getLink() {
        return link;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getCategory() {
        return category;
    }

}
