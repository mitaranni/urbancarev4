package com.kominfo.lenovo.ncc_smartcity.event;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;

import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kominfo.lenovo.ncc_smartcity.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Dimas Maulana on 5/26/17.
 * Email : araymaulana66@gmail.com
 */

public class CustomAdapterEventAnda extends RecyclerView.Adapter<CustomAdapterEventAnda.MahasiswaViewHolder> {

    private ArrayList<DataModel> dataList;

    public CustomAdapterEventAnda(ArrayList<DataModel> dataList) {
        this.dataList = dataList;
    }

    static public void shareImage(String url, final String teks, final Context context) {
        Picasso.with(context).load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("image/*");
                i.putExtra(Intent.EXTRA_TEXT, teks);
                i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap, context));
                context.startActivity(Intent.createChooser(i, "Share Image"));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });
    }

    static public Uri getLocalBitmapUri(Bitmap bmp, Context context) {
        Uri bmpUri = null;
        try {

            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_.png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }


    @Override
    public MahasiswaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.event_cardview_add, parent, false);
        return new MahasiswaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MahasiswaViewHolder holder, final int position) {
        holder.txtTitle.setText(dataList.get(position).getTitle());
        holder.txtCategory.setText(dataList.get(position).getCategory());
        holder.txtDescription.setText(dataList.get(position).getDescription());
//        holder.txtLink.setText(dataList.get(position).getLink());
//        holder.txtImg.setImageURI(Uri.parse(dataList.get(position).getImg()));
        byte[] decodedString = Base64.decode(dataList.get(position).getImg(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        holder.txtImg.setImageBitmap(decodedByte);
//        Picasso.with(holder.itemView.getContext()).load(dataList.get(position).getImg().replace("150x150", "")).into(holder.txtImg);
        holder.btnShare.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                shareImage(dataList.get(position).getImg(), dataList.get(position).getTitle()
                        + "\n" + dataList.get(position).getLink(), v.getContext());


            }
        });
        holder.txtImg.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                final Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.event_dialog_lihat);
                dialog.setTitle("Poster");
                PhotoView image = (PhotoView) dialog.findViewById(R.id.image);
                Picasso.with(v.getContext()).load(dataList.get(position).getImg().replace("-150x150", "")).into(image);
                dialog.show();
            }
        });
        holder.btnLihat.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(dataList.get(position).getLink()));
                v.getContext().startActivity(browserIntent);


            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class MahasiswaViewHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle, txtLink, txtCategory, txtDescription;
        private ImageView txtImg;
        private Button btnLihat, btnShare, btnDelete;

        public MahasiswaViewHolder(View itemView) {
            super(itemView);

            txtTitle = itemView.findViewById(R.id.list_title);
            txtLink = itemView.findViewById(R.id.list_link);
            txtCategory = itemView.findViewById(R.id.list_category);
            txtDescription = itemView.findViewById(R.id.list_description);
            txtImg = itemView.findViewById(R.id.list_image);
            btnShare = itemView.findViewById(R.id.list_share);
            btnLihat = itemView.findViewById(R.id.list_lihat);
            btnDelete = itemView.findViewById(R.id.list_delete);
        }
    }
}