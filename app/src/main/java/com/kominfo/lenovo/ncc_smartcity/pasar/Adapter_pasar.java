package com.kominfo.lenovo.ncc_smartcity.pasar;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kominfo.lenovo.ncc_smartcity.R;

import java.util.List;

/**
 * Created by arimahardika on 23/02/2018.
 */

public class Adapter_pasar extends RecyclerView.Adapter<Adapter_pasar.PasarViewHolder> {

    private static int currentPosition = 0;
    private List<Model_Pasar> listPasar;
    //dont delete, used in Pasar.java
    private Context context;

    public Adapter_pasar(List<Model_Pasar> listPasar, Context context) {
        this.listPasar = listPasar;
        this.context = context;
    }

    @Override
    public PasarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pasar_list_row, parent, false);
        return new PasarViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PasarViewHolder holder, int position) {
        Model_Pasar model_pasar = listPasar.get(position);
        String nama = model_pasar.getNama().isEmpty() ? model_pasar.getNama() : model_pasar.getNama() + " -";
        String getNama = nama + model_pasar.getJenis();
        CharSequence getSatuan = model_pasar.getSatuan();
        CharSequence getHargaKemarin = model_pasar.getHarga_kemarin();
        String[] tanggal = model_pasar.getTanggal().split("-");
        String tanggalBaru = tanggal[2] + "-" + tanggal[1] + "-" + tanggal[0];
        holder.txt_nama.setText(getNama.trim());
        holder.txt_satuan.setText(getSatuan);
        holder.txt_harga_kemarin.setText(getHargaKemarin);
        holder.txt_tanggal.setText(tanggalBaru);
    }

    @Override
    public int getItemCount() {
        return listPasar.size();
    }

    public class PasarViewHolder extends RecyclerView.ViewHolder {

        TextView txt_nama, txt_satuan, txt_harga_kemarin, txt_tanggal;

        LinearLayout linearLayout;

        public PasarViewHolder(View itemView) {
            super(itemView);

            txt_nama = itemView.findViewById(R.id.tv_pasar_nama);
            txt_satuan = itemView.findViewById(R.id.tv_pasar_satuan);
            txt_harga_kemarin = itemView.findViewById(R.id.tv_pasar_harga);
            txt_tanggal = itemView.findViewById(R.id.tv_pasar_tanggal);

            linearLayout = itemView.findViewById(R.id.ll_pasar_linearLayout);
        }
    }


}
