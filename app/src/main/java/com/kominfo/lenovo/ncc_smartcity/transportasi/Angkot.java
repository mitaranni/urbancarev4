package com.kominfo.lenovo.ncc_smartcity.transportasi;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.kominfo.lenovo.ncc_smartcity.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;

public class Angkot extends AppCompatActivity {

    private static final String TAG = Angkot.class.getSimpleName();

    private final static int REQUEST_CODE = 42;

    public static final int PERMISSION_CODE = 42042;

    public static final String SAMPLE_FILE = "angkot.pdf";

    public static final String READ_EXTERNAL_STORAGE = "android.permission.READ_EXTERNAL_STORAGE";

    private PDFView pdfView;

    private Button openPdf;

    private Uri uri;

    private Integer pageNumber = 0;

    private String pdfFileName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_angkot);
        pdfView = (PDFView) findViewById(R.id.pdfView);
        pdfView.fromAsset("angkot.pdf")
                .enableSwipe(true)
                 .defaultPage(pageNumber)
                .enableAnnotationRendering(true)
                .scrollHandle(new DefaultScrollHandle(this))
                .spacing(10) // in dp
                .load();
    }

}
