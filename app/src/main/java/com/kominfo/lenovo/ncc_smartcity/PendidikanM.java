package com.kominfo.lenovo.ncc_smartcity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.kominfo.lenovo.ncc_smartcity.pendidikan.PerguruanTinggi;
import com.kominfo.lenovo.ncc_smartcity.pendidikan.Perpustakaan;
import com.kominfo.lenovo.ncc_smartcity.pendidikan.Pondok;
import com.kominfo.lenovo.ncc_smartcity.pendidikan.Sd;
import com.kominfo.lenovo.ncc_smartcity.pendidikan.Sma;
import com.kominfo.lenovo.ncc_smartcity.pendidikan.Smp;

public class PendidikanM extends Activity{

    CardView sd, smp, sma, pesantren, pt, perpus;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_pendidikan);

        sd = findViewById(R.id.cardView_sd);
        smp = findViewById(R.id.cardView_smp);
        sma = findViewById(R.id.cardView_sma);
        pesantren = findViewById(R.id.cardView_pondok);
        pt = findViewById(R.id.cardView_pt);
        perpus = findViewById(R.id.cardView_perpus);

        sd = (CardView) findViewById(R.id.cardView_sd);
        sd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PendidikanM.this, Sd.class);
                startActivity(i);
            }
        });
        smp = (CardView) findViewById(R.id.cardView_smp);
        smp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PendidikanM.this, Smp.class);
                startActivity(i);
            }
        });
        sma = (CardView) findViewById(R.id.cardView_sma);
        sma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PendidikanM.this, Sma.class);
                startActivity(i);
            }
        });
        pesantren = (CardView) findViewById(R.id.cardView_pondok);
        pesantren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PendidikanM.this, Pondok.class);
                startActivity(i);
            }
        });
        pt = (CardView) findViewById(R.id.cardView_pt);
        pt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PendidikanM.this, PerguruanTinggi.class);
                startActivity(i);
            }
        });
        perpus = (CardView) findViewById(R.id.cardView_perpus);
        perpus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PendidikanM.this, Perpustakaan.class);
                startActivity(i);
            }
        });

    }


}
