package com.kominfo.lenovo.ncc_smartcity.surat;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.kominfo.lenovo.ncc_smartcity.R;

public class Main_ktp extends Activity {

    TextView textView;
    Typeface customFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ktp);

        customFont = Typeface.createFromAsset(getAssets(), "font/MontserratBold.ttf");
        textView = findViewById(R.id.textViewktp);
        textView.setTypeface(customFont);

    }
}
