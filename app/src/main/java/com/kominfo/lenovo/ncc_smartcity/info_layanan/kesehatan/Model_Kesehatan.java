package com.kominfo.lenovo.ncc_smartcity.info_layanan.kesehatan;

/**
 * Created by arimahardika on 06/03/2018.
 */

public class Model_Kesehatan {
    private String tipe, nama, jalan, telp;

    public Model_Kesehatan(String tipe, String nama, String jalan, String telp) {
        this.tipe = tipe;
        this.nama = nama;
        this.jalan = jalan;
        this.telp = telp;
    }

    public String getTipe() {
        return tipe;
    }

    public String getNama() {
        return nama;
    }

    public String getJalan() {
        return jalan;
    }

    public String getTelp() {
        return telp;
    }
}
