package com.kominfo.lenovo.ncc_smartcity.cek_tagihan;//package com.example.lenovo.ncc_smartcity.cek_tagihan;
//
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//
//import com.example.lenovo.ncc_smartcity.R;
//import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management.SendDataTagihan;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//import retrofit2.Retrofit;
//import retrofit2.converter.scalars.ScalarsConverterFactory;
//
//
//public class CekPbb extends AppCompatActivity {
//
//    private EditText et_nop, et_nama;
//    private Button btn,btn_hapus;
//    public static String nop, namaa;
//    TextView tvNop,tvNama, tvAlamat;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_cek_pbb);
//
//        et_nop = findViewById(R.id.et_nomorpbb);
//        et_nama = findViewById(R.id.et_namapbb);
//        tvNop = findViewById(R.id.tv_nop);
//        tvNama = findViewById(R.id.tv_nama);
//        tvAlamat = findViewById(R.id.tv_alamat);
//        btn = findViewById(R.id.btn_cekTagihan);
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                tagihanPbb();
//            }
//        });
//
//        btn_hapus = findViewById(R.id.btn_hapus);
//        btn_hapus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                et_nop.setText("");
//                et_nama.setText("");
//                tvNama.setText("");
//                tvAlamat.setText("");
//                tvNop.setText("");
//
//            }
//        });
//    }
//
//    private void tagihanPbb() {
//
//        final String nop = et_nop.getText().toString().trim();
//        final String nama = et_nama.getText().toString().trim();
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(SendDataTagihan.JSONGETURL)
//                .addConverterFactory(ScalarsConverterFactory.create())
//                .build();
//
//        SendDataTagihan api = retrofit.create(SendDataTagihan.class);
//
//        Map<String, String> mapdata = new HashMap<>();
//        mapdata.put("nop", nop);
//        mapdata.put("nama", nama);
//
//        Call<String> call = api.getPbb(mapdata);
//
//        call.enqueue(new Callback<String>() {
//            @Override
//            public void onResponse(Call<String> call, Response<String> response) {
//                Log.i("Responsestring", response.body().toString());
//                //Toast.makeText()
//                if (response.isSuccessful()) {
//                    if (response.body() != null) {
//                        Log.i("onSuccess", response.body().toString());
//
//                        String jsonresponse = response.body().toString();
//                        parseData(jsonresponse);
//
//
//                    } else {
//                        Log.i("onEmptyResponse", "Returned empty response");
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<String> call, Throwable t) {
//
//            }
//        });
//
//    }
//
//    public void parseData(String response) {
//
//        try {
//            JSONObject jsonObject = new JSONObject(response);
//            if (jsonObject.getString("status").equals("true")) {
//
//                JSONArray dataArray = jsonObject.getJSONArray("data");
//                for (int i = 0; i < dataArray.length(); i++) {
//
//                    JSONObject dataobj = dataArray.getJSONObject(i);
//                    String nop = dataobj.getString("nop");
//                    String namaa = dataobj.getString("nama_wp");
//                    String alamatt = dataobj.getString("alamat_op");
//
//                    tvNop.setText(nop);
//                    tvNama.setText(namaa);
//                    tvAlamat.setText(alamatt);
//                }
//
//
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//
//
//}
