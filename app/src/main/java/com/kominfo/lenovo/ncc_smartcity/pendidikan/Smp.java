package com.kominfo.lenovo.ncc_smartcity.pendidikan;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.kominfo.lenovo.ncc_smartcity.R;

public class Smp extends AppCompatActivity {

    WebView webVPe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infopendidikan);

        webVPe = (WebView)findViewById(R.id.webVP);
        String url = "ncctrial.malangkota.go.id/f_kios_v2/beranda/pendidikan/a723ff721d91d9f34309debc39b7082f69c6b9990eb1c4aff93361aeec412bcb?menu=0b7dc2a6fe4ed1b3d9180a291bdd3de17d670689cf143d89625d90496370e94c22551e030972471b0f33a959bad3dea6445f26353a9b0567c6fb06fa8efe7bde";
        webVPe.getSettings().setJavaScriptEnabled(true);
        webVPe.setFocusable(true);
        webVPe.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webVPe.getSettings().setDomStorageEnabled(true);
        webVPe.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webVPe.getSettings().setDatabaseEnabled(true);
        webVPe.getSettings().setAppCacheEnabled(true);
        webVPe.loadUrl(url);
        webVPe.setWebViewClient(new WebViewClient());
    }

    @Override
    public void onBackPressed() {
        if (webVPe.canGoBack()) {
            webVPe.goBack();
        }else {
            super.onBackPressed();
        }
    }
}

