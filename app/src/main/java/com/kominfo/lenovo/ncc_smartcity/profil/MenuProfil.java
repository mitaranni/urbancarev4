package com.kominfo.lenovo.ncc_smartcity.profil;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kominfo.lenovo.ncc_smartcity.MenuBaru;
import com.kominfo.lenovo.ncc_smartcity.R;
import com.kominfo.lenovo.ncc_smartcity._globalVariable.MessageVariabe;
import com.kominfo.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.kominfo.lenovo.ncc_smartcity.internalLib.CheckConn;
import com.kominfo.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management.SendDataLogin;
import com.kominfo.lenovo.ncc_smartcity.internalLib.Session;
import com.kominfo.lenovo.ncc_smartcity.internalLib.SessionCheck;
import com.kominfo.lenovo.ncc_smartcity.internalLib.TextSafety;
import com.kominfo.lenovo.ncc_smartcity.internalLib.refresh_activity.RefreshDataUser;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class MenuProfil extends AppCompatActivity {
    String TAG = "MenuProfil";
    CircleImageView img;

    String BASE_URL = URLCollection.DATA_SOURCE_LOKER;
    String TOKEN_MOBILE = "X00W";


    TextView nik, username, email, tlp, type_penduduk, nama, alamat, tgl_lhr, tmp_lhr, jk, wn, pass;
    TextView changePhoto, logOut, namaUser, verifikasi;
    LinearLayout ll_editAll;
    Button btnEdit, btnChaPhoto, btnLogout, btnChPass;
    Button btnSave, btnCancle, btnRefresh;
    SwipeRefreshLayout swpRefresh;

    String nik_ = "", username_ = "", email_ = "", tlp_ = "", status_ = "", url_img_ = "", pass_ = "",
            nama_ = "", alamat_ = "", tgl_lhr_ = "", tmp_lhr_ = "", jk_ = "", wn_ = "";

    String val_type, val_status, val_jk, val_wn;
    String val_type_penduduk = "";
    String user_id = "";

    ProgressDialog progressDialog;
    Intent intent;
    Call<ResponseBody> getdata;
    SendDataLogin base_url_management;
    //safety text
    TextSafety textSafety = new TextSafety();
    CheckConn checkConn = new CheckConn();
    Session session = new Session();

    int status_verifikasi = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_profil);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btnRefresh = (Button) findViewById(R.id.btnLogout);
        img = (CircleImageView) findViewById(R.id.img_profil);
        nama = (TextView) findViewById(R.id.val_email);
        alamat = (TextView) findViewById(R.id.val_alamat);
        tgl_lhr = (TextView) findViewById(R.id.val_tgl_lhr);
        img = (CircleImageView) findViewById(R.id.img_profil);
        int status_call = 0;
        get_data();

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               session.sessionDestroy(MenuProfil.this);
                intent = new Intent(MenuProfil.this, MenuBaru.class);
               startActivity(intent);
                finish();
            }
        });
    }

    public void get_data() {
        String[] data_session = new SessionCheck().seesionLoginChecker(MenuProfil.this);
        Log.e(TAG, "onClick: " + data_session[1]);
        if (data_session != null) {

            user_id = data_session[1];
            username_ = data_session[2];
            url_img_ = data_session[3];
            nik_ = data_session[4];
            email_ = data_session[5];
            tlp_ = data_session[6];
            status_ = data_session[7];
            val_type_penduduk = data_session[8];

            nama_ = data_session[9];
            alamat_ = data_session[10];
            wn_ = data_session[11];
            tmp_lhr_ = data_session[12];
            tgl_lhr_ = data_session[13];
            jk_ = data_session[14];


            if (Integer.parseInt(jk_) == 0) {
                val_jk = "Laki-Laki";
            } else {
                val_jk = "Perempuan";
            }

            if (Integer.parseInt(wn_) == 0) {
                val_wn = "WNI";
            } else {
                val_wn = "WNA";
            }

            if (Integer.parseInt(val_type_penduduk) == 0) {
                val_type = "Penduduk Malang";
            } else {
                val_type = "Bukan Penduduk Malang";
            }
            alamat = (TextView) findViewById(R.id.val_alamat);
            tgl_lhr = (TextView) findViewById(R.id.val_tgl_lhr);
            email = (TextView) findViewById(R.id.val_email);
            if (!url_img_.isEmpty()) {
                Log.e(TAG, "get_data: url_img_: " + url_img_);
                try {
//                    Picasso.with(activity).load(mayorShipImageLink).transform(new CircleTransform()).into(ImageView);
                    Log.e(TAG, "get_data: " + BASE_URL + url_img_);
                    Picasso.with(MenuProfil.this)
                            .load(new URLCollection().BASE_URL_FOTO + url_img_)
                            .error(R.drawable.icon_razer_50)
                            .into(img);
                } catch (Exception e) {
                    Log.e(TAG, "get_data: img error");
                }

            }

        }
    }

    private void sendData() {
        progressDialog = new ProgressDialog(MenuProfil.this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };

        progressDialog.setMessage(MessageVariabe.MESSAGE_LOADING);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();


    }

    public void setData(final Call<ResponseBody> getdata, final int status_call) {
        Log.e(TAG, "adapterRequest: run");

        getdata.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                progressDialog.dismiss();
                JSONObject dataJson = null;
                String message_out = null;
                try {
                    //Log.e(TAG, "onResponse: "+response.body().string());
                    dataJson = new JSONObject(response.body().string());
                    Log.e(TAG, "onResponse: " + dataJson);
                    if (dataJson != null) {
                        String status = dataJson.getJSONObject("body_message")
                                .getString("status");
                        message_out = dataJson.getJSONObject("body_message")
                                .getString("message");
                        if (Boolean.valueOf(status)) {
                            new RefreshDataUser().create_session(dataJson.getJSONObject("body"), MenuProfil.this);

                            get_data();//finish();
                        }
//                        switch (status_call) {
//                            case 1:
//                                //refersh
//                                set_new_session(dataJson);
//                                get_data();
//                                break;
//                            case 2:
//                                String status = dataJson.getJSONObject("body_message")
//                                        .getString("status");
//                                if (Boolean.valueOf(status)) {
//                                    new RefreshDataUser().create_session(dataJson.getJSONObject("body"), MainAkun.this);
//                                    finish();
//                                }
//                                //Log.e(TAG, "onResponse: "+dataJson.toString());
//                                //update
//                                break;
//                        }
                        Toast.makeText(MenuProfil.this, message_out, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "onResponse: IOException: " + e.getMessage());
                } catch (JSONException e) {
                    Log.e(TAG, "onResponse: JSONException: " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                progressDialog.dismiss();
                Toast.makeText(MenuProfil.this, MessageVariabe.MESSAGE_ERROR_RESPONSE_FAIL_SERVER, Toast.LENGTH_LONG).show();
            }
        });

    }


    private void set_new_session(JSONObject dataJson) {
//        Log.e(TAG, "set_new_session: " + dataJson);
        JSONObject dataMessage = null;
        try {
            dataMessage = dataJson.getJSONObject("body_message");
            String status_message = dataMessage.getString("status");

            if (status_message != null && Boolean.valueOf(status_message) == true) {
                JSONObject dataMain = dataJson.getJSONObject("body");
//                Log.e(TAG, "set_new_session: "+dataMain);
                new RefreshDataUser().create_session(dataMain, MenuProfil.this);
            }
        } catch (JSONException e) {
            Log.e(TAG, "set_new_session: " + e.getMessage());
        }


    }

}
