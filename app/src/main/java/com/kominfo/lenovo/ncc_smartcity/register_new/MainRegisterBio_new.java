package com.kominfo.lenovo.ncc_smartcity.register_new;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kominfo.lenovo.ncc_smartcity.R;
import com.kominfo.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.kominfo.lenovo.ncc_smartcity.internalLib.CheckConn;
import com.kominfo.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management.SendDataLogin;
import com.kominfo.lenovo.ncc_smartcity.internalLib.TextSafety;
import com.kominfo.lenovo.ncc_smartcity.internalLib.register.MainRegisterData;
import com.kominfo.lenovo.ncc_smartcity.internalLib.register.SecondRegisterData;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by USER on 23/03/2018.
 */

public class MainRegisterBio_new extends FragmentActivity {

    String BASE_URL = URLCollection.DATA_SOURCE_LOKER;
    String TOKEN_MOBILE = "X00W";
    String status_warga = "",
            TAG = "MainRegisterBio_new",
            headerTi = "Register Warga Malang";
    Intent intent;

    TextView tv_header;
    EditText eNik, eEmail, ePass, eRePass, eUsernm, eTlp;
    Button btnReg, btnPrev;
    TextSafety textSafety = new TextSafety();
    Typeface customFont;


    ProgressDialog progressDialog;

    Call<ResponseBody> getdata;

    SendDataLogin base_url_management;

    String message_galat = "inputkan data dengan benar",
            response_galat = "failed input data, coba lagi nanti";

    //check internet access
    CheckConn checkConn = new CheckConn();

    MainRegisterData tempMainRegData = new MainRegisterData();
    SecondRegisterData tempSecondRegData = new SecondRegisterData();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_main1_wrgmlg);

        //Log.e(TAG, "onCreate: "+status_warga);
        customFont = Typeface.createFromAsset(getAssets(), "font/myriad_pro_regular.otf");


        eNik = (EditText) findViewById(R.id.eNik);
        eEmail = (EditText) findViewById(R.id.eEmail);
        eUsernm = (EditText) findViewById(R.id.eUsrNm);
        eTlp = (EditText) findViewById(R.id.eTlp);
        ePass = (EditText) findViewById(R.id.ePassword);
        eRePass = (EditText) findViewById(R.id.eRePassword);

        btnReg = (Button) findViewById(R.id.btnNext);
        btnPrev = (Button) findViewById(R.id.btnPrev);

        tv_header = (TextView) findViewById(R.id.tv_header);
        tv_header.setTypeface(customFont);

        String[] dataSession = tempMainRegData.MainRegisterGet(this);
        if (dataSession != null) {
            eNik.setText(dataSession[0]);
            eUsernm.setText(dataSession[1]);
            eEmail.setText(dataSession[2]);
            eTlp.setText(dataSession[3]);
            status_warga = dataSession[4];

            Log.e(TAG, "onCreate: data not null");
        } else {
            intent = getIntent();
            status_warga = intent.getStringExtra("status_warga");

            Log.e(TAG, "onCreate: data null");
        }


        if (Integer.parseInt(status_warga) == 1) {
            headerTi = "Register Warga Non Malang";
        }

        tv_header.setText(headerTi);

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ePass.getText().toString().equals(eRePass.getText().toString())) {
                    if (checkNullAble()) {
                        Log.e(TAG, "onClick: run");
                        tempMainRegData.MainRegisterSet(MainRegisterBio_new.this,
                                eNik.getText().toString(),
                                eUsernm.getText().toString(),
                                eTlp.getText().toString(),
                                eEmail.getText().toString(),
                                status_warga,
                                ePass.getText().toString());
                        Log.e(TAG, "onCreate: " + status_warga);

                        if (Integer.parseInt(status_warga) == 0) {
                            intent = new Intent(MainRegisterBio_new.this, MainRegisterFixed.class);
                        } else {
                            intent = new Intent(MainRegisterBio_new.this, MainRegisterBioDetail_new.class);
                        }
                        intent.putExtra("status_warga", status_warga);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(MainRegisterBio_new.this,
                                message_galat, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tempMainRegData.MainRegisterDestroy(MainRegisterBio_new.this);
                tempSecondRegData.SecondRegisterDestroy(MainRegisterBio_new.this);

                Log.e(TAG, "onClick: destroy");
                intent = new Intent(MainRegisterBio_new.this, MainRegister_new.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public Boolean checkNullAble() {
        if (textSafety.get_valid_number(eNik.getText().toString()) == true
                && textSafety.get_valid_number(eTlp.getText().toString()) == true
                && textSafety.get_valid_char(eUsernm.getText().toString()) == true
                && textSafety.get_valid_char(ePass.getText().toString()) == true) {

            Log.e(TAG, "checkNullAble: " + true);
            return true;
        }
        return false;
    }
}
