package com.kominfo.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by USER on 22/03/2018.
 */

public interface SendDataLogin {

    //---------------------------------------login activity----------------------------------------

    @FormUrlEncoded
    @POST("user/Getiden/get_iden")
    Call<ResponseBody> loginRequest(@Field("usrnm") String usrnm,
                                    @Field("pass") String pass,
                                    @Field("token") String token);


    //EditText eNik, eEmail, ePass, eRePass, eUsernm, eTlp;

    //---------------------------------------upload foto----------------------------------------

    @Multipart
    @POST("user/Sendregisternew/sendphoto")
    Call<ResponseBody> RegisterUpdateImg(@Part("id_user") RequestBody id_user,
                                         @Part("token") RequestBody token,
                                         @Part("action_") RequestBody action_,
                                         @Part MultipartBody.Part file);

    //---------------------------------------register----------------------------------------
    @FormUrlEncoded
    @POST("user/Sendregisternew/register_user")
    Call<ResponseBody> RegisterRequestMlg(@Field("nik") String nik,
                                          @Field("username") String username,
                                          @Field("password") String password,
                                          @Field("email") String email,
                                          @Field("tlp") String tlp,
                                          @Field("type_penduduk") String type_penduduk,
                                          @Field("token") String token);

    @FormUrlEncoded
    @POST("user/Sendregisternew/register_user")
    Call<ResponseBody> RegisterRequestNoMlg(@Field("nik") String nik,
                                            @Field("username") String username,
                                            @Field("password") String password,
                                            @Field("email") String email,
                                            @Field("tlp") String tlp,
                                            @Field("type_penduduk") String type_penduduk,

                                            @Field("nama") String nama,
                                            @Field("jk") String jk,
                                            @Field("alamat") String alamat,
                                            @Field("kwn") String kwn,
                                            @Field("tmp_lahir") String tmp_lahir,
                                            @Field("tgl_lahir") String tgl_lahir,

                                            @Field("token") String token);

    //---------------------------------------update activity----------------------------------------

    @FormUrlEncoded
    @POST("user/Manageakun/update_email")
    Call<ResponseBody> UpdateEmail(@Field("id_user") String id_user,
                                   @Field("email") String email,
                                   @Field("token") String token,
                                   @Field("action") String action);

    @FormUrlEncoded
    @POST("user/Manageakun/update_pass")
    Call<ResponseBody> UpdatePass(@Field("id_user") String id_user,
                                  @Field("pass") String pass,
                                  @Field("pass_old") String pass_old,
                                  @Field("token") String token);

    @FormUrlEncoded
    @POST("user/Manageakun/main_update_data")
    Call<ResponseBody> UpdateDataAll(@Field("id_user") String id_user,
                                     @Field("username") String username,
                                     @Field("tlp") String tlp,
                                     @Field("token") String token);

    //---------------------------------------refresh activity----------------------------------------

    @FormUrlEncoded
    @POST("user/Getiden/get_iden_refresh")
    Call<ResponseBody> RefreshDash(@Field("id_user") String id_user,
                                   @Field("token") String token);

    //---------------------------------------vertification----------------------------------------

    @FormUrlEncoded
    @POST("user/Managever/set_manual_email_vert")
    Call<ResponseBody> VerifikasiEmail(@Field("id_user") String id_user,
                                       @Field("token") String token);

    @Multipart
    @POST("user/Managever/set_ktp_vert")
    Call<ResponseBody> verifikasiKTP(@Part("id_user") RequestBody id_user,
                                     @Part("token") RequestBody token,
                                     @Part MultipartBody.Part file);
}
