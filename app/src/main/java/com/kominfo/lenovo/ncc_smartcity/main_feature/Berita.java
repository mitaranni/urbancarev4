package com.kominfo.lenovo.ncc_smartcity.main_feature;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import com.kominfo.lenovo.ncc_smartcity.berita.Item;
import com.kominfo.lenovo.ncc_smartcity.berita.ItemAdapter;
import com.kominfo.lenovo.ncc_smartcity.R;

public class Berita extends AppCompatActivity {

    ProgressDialog progressDialog;
    String loadingMessage = "Loading";
    Typeface customFont;

    private RecyclerView recyclerView;
    private ItemAdapter adapter_item;
    private ArrayList<Item> itemArrayList = new ArrayList<>();

    public static Drawable LoadImageFromWebOperations(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable drawable = Drawable.createFromStream(is, "src name");
            return drawable;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_berita);

        customFont = Typeface.createFromAsset(getAssets(), "font/myriad_pro_regular.otf");
        TextView textView = findViewById(R.id.textView_berita);
        textView.setTypeface(customFont);

        getWebsite();

    }

    private void getWebsite() {

        progressDialog = new ProgressDialog(this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };
        progressDialog.setMessage(loadingMessage);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        new Thread(new Runnable() {

            public void run() {
                final StringBuilder builder = new StringBuilder();

                try {
                    Document doc = Jsoup.connect("http://suryamalang.tribunnews.com/rss").get();

                    Elements links = doc.select("item");

                    for (Element link : links) {

                        String link_text = link.select("link").text();
                        String title_text = link.select("title").text();
                        String img_text = link.select("enclosure[url]").attr("url");
                        String description = Jsoup.parse(link.select("Layanan_description").text()).text();

                        HashMap<String, String> dumb_data = new HashMap<>();

                        dumb_data.put("title", title_text);
                        dumb_data.put("link", link_text);
                        dumb_data.put("img", String.valueOf(LoadImageFromWebOperations(img_text)));
                        dumb_data.put("Layanan_description", img_text);

                        try {
                            itemArrayList.add(new Item(title_text, img_text, description, "read more", link_text));
                        } catch (Exception e) {
                            Log.e("error_data", "run: " + e);
                        }
                    }
                    Log.e("data", "run: " + itemArrayList);
                } catch (IOException e) {
                    builder.append("Error : ").append(e.getMessage()).append("\n");
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        recyclerView = findViewById(R.id.recycler_view);
                        adapter_item = new ItemAdapter(itemArrayList);
                        Log.e("adapter_item", "run: " + itemArrayList);

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Berita.this);

                        recyclerView.setLayoutManager(layoutManager);

                        recyclerView.setAdapter(adapter_item);
                        recyclerView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.e("view", "onClick: " + v);
                            }
                        });
                    }
                });
                progressDialog.dismiss();
            }
        }).start();
    }
}
