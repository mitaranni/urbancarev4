package com.kominfo.lenovo.ncc_smartcity.event;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kominfo.lenovo.ncc_smartcity.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by lenovo on 2/5/2018.
 */

public class EventProfile extends AppCompatActivity {

    //    ListView listView;
    private static CustomAdapterEventAnda adapter;
    ArrayList<DataModel> dataModels;
    ProgressDialog progressDialog;
    String loadingText = "Loading";
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24px));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://keberhasilansmart-spiritbro.c9users.io/smartcity/bacaevent";

        progressDialog = new ProgressDialog(this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };
        //picolo
        progressDialog.setMessage(loadingText);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.e("apa", response);
                        progressDialog.dismiss();

                        try {
                            dataModels = new ArrayList<>();
                            JSONArray panda = new JSONArray(response);
                            for (int i = 0; i < panda.length(); i++) {
                                JSONObject panda1 = panda.getJSONObject(i);
                                //image dah jadi besar kalau dishare
                                dataModels.add(new DataModel(panda1.getString("gambar"),
                                        "",
                                        panda1.getString("nama"),
                                        panda1.getString("deskripsi"),
                                        panda1.getString("kategori")));

                            }
//                           adapter= new CustomAdapter(dataModels,getApplicationContext());
//
//                            listView.setAdapter(adapter);
                            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

                            adapter = new CustomAdapterEventAnda(dataModels);

                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(EventProfile.this);

                            recyclerView.setLayoutManager(layoutManager);

                            recyclerView.setAdapter(adapter);

                            //mTextView.setText(panda1.getString("img"));
                        } catch (JSONException e) {
                            Log.e("panda", e.toString());
                            Toast.makeText(EventProfile.this, "Error, something went wrong!", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                        // Getting JSON Array node

//                        mTextView.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                mTextView.setText("That didn't work!");
                Log.e("apa", error.toString());
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    //hai
    @Override
    protected void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        super.onDestroy();
    }

}
