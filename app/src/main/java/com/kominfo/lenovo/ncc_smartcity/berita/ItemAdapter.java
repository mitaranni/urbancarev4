package com.kominfo.lenovo.ncc_smartcity.berita;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.kominfo.lenovo.ncc_smartcity.R;

/**
 * Created by USER on 20/02/2018.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {


    private ArrayList<Item> dataList;
    private Context context;

    public ItemAdapter(Context context) {
        this.context = context;
    }

    public ItemAdapter(ArrayList<Item> dataList) {
        this.dataList = dataList;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.berita_item_recycler, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        Log.e("holder", "onBindViewHolder: " + holder);
        holder.title.setText(dataList.get(position).getTitle());

        Picasso.with(holder.itemView.getContext()).load(dataList.get(position).getImage()).into(holder.image);
        //holder.image.setText(dataList.get(position).getImage());
        holder.descripton.setText(dataList.get(position).getDescription());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(view.getContext(), MainWeb.class);
////                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
//                intent.putExtra("link_detail", dataList.get(position).getLink());
//                intent.putExtra("hostname", "www.malangkota.go.id");
//                view.getContext().startActivity(intent);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(dataList.get(position).getLink()));
                view.getContext().startActivity(browserIntent);
            }
        });

        holder.read.setText(dataList.get(position).getReadmore());
        holder.read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(dataList.get(position).getLink()));
                v.getContext().startActivity(browserIntent);
//
//                Intent intent = new Intent(v.getContext(), MainWeb.class);
////                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
//                intent.putExtra("link_detail", dataList.get(position).getLink());
//                intent.putExtra("hostname", "www.malangkota.go.id");
//                v.getContext().startActivity(intent);
//
//                Log.e("intent", "onClick: " + intent);
//                Log.e("link", "onClick: " + dataList.get(position).getLink());

            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        private TextView title, descripton, read;
        private ImageView image;

        public ItemViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView_display_berita);
            title = itemView.findViewById(R.id.txt_title);
            image = itemView.findViewById(R.id.txt_image);
            descripton = itemView.findViewById(R.id.txt_desc);
            read = itemView.findViewById(R.id.txt_read);
        }


    }
}
