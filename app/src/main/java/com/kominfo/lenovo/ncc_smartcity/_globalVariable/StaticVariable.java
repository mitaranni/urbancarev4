package com.kominfo.lenovo.ncc_smartcity._globalVariable;

/**
 * Created by lenovo on 2/6/2018.
 */

public class StaticVariable {
    public final static String NAMA_LAYANAN = "NAMA_LAYANAN";
    public final static String SPECIFIC_FEATURE = "SPECIFIC_FEATURE";

    public final static String KEPENDUDUKAN = "KEPENDUDUKAN";
    public final static String PAJAK = "PAJAK";
    public final static String PERIZINAN = "PERIZINAN";
    public final static String KESEHATAN = "KESEHATAN";
    public final static String WISATA = "WISATA";

    public final static String INSTANSI = "INSTANSI";
    public final static String DISPLAY_INSTANSI = "Instansi Pelayanan Kependudukan";

    public final static String PELAYANAN_PAJAK = "ALAMAT INSTANSI";
    public final static String DISPLAY_PELAYANAN_PAJAK = "Alamat Pelayanan Pajak";

    public final static String DINAS_KESEHATAN = "DINAS_KESEHATAN";
    public final static String DISPLAY_DINAS_KESEHATAN = "Alamat Dinas Kesehatan";

    public final static String DINAS_PERIZINAN = "DINAS_PERIZINAN";
    public final static String DISPLAY_DINAS_PERIZINAN = "Alamat Dinas Perizinan";

    //<editor-fold desc="KEPENDUDUKAN">

    //<editor-fold desc="LIST FITUR">
    public final static String KARTU_KELUARGA = "KARTU_KELUARGA";
    public final static String KTP = "KTP";
    public final static String AKTA = "AKTA";
    //</editor-fold>

    //<editor-fold desc="VALUE">
    public final static String KK_BARU = "KK_BARU";
    public final static String KK_HILANG_RUSAK = "KK_HILANG_RUSAK";
    public final static String KK_TAMBAH_ANGGOTA = "KK_TAMBAH_ANGGOTA";
    public final static String KK_PENGURANGAN_ANGGOTA = "KK_PENGURANGAN_ANGGOTA";
    public final static String KK_PERUBAHAN_ANGGOTA = "KK_PERUBAHAN_ANGGOTA";

    public final static String KTP_BARU = "KTP_BARU";
    public final static String KTP_HILANG_RUSAK = "KTP_HILANG_RUSAK";
    public final static String KTP_SALAH_DIRUBAH = "KTP_SALAH_DIRUBAH";

    public final static String AKTA_KELAHIRAN = "AKTA_KELAHIRAN";
    public final static String AKTA_KEMATIAN = "AKTA_KEMATIAN";
    public final static String AKTA_PERKAWINAN = "AKTA_PERKAWINAN";
    public final static String AKTA_PENCATATAN_PERUBAHAN_NAMA = "AKTA_PENCATATAN_PERUBAHAN_NAMA";
    public final static String AKTA_PENCATATAN_PENGAKUAN_ANAK = "AKTA_PENCATATAN_PENGAKUAN_ANAK";
    public final static String KUTIPAN_KEDUA_AKTA_PENCATATAN_SIPIL = "KUTIPAN_KEDUA_AKTA_PENCATATAN_SIPIL";
    public final static String AKTA_ADOPSI = "AKTA_ADOPSI";
    public final static String AKTA_PENGESAHAN_ANAK = "AKTA_PENGESAHAN_ANAK";
    //</editor-fold>

    //<editor-fold desc="DISPLAY">
    public final static String DISPLAY_KARTU_KELUARGA = "Kartu Keluarga";
    public final static String DISPLAY_KTP = "KTP";
    public final static String DISPLAY_AKTA = "Akta";

    public final static String DISPLAY_KK_BARU = "Kartu Keluarga Baru";
    public final static String DISPLAY_KK_HILANG_RUSAK = "Kartu Keluarga Hilang / Rusak";
    public final static String DISPLAY_KK_TAMBAH_ANGGOTA = "Tambah Anggota";
    public final static String DISPLAY_KK_PENGURANGAN_ANGGOTA = "Pengurangan Anggota";
    public final static String DISPLAY_KK_PERUBAHAN_ANGGOTA = "Perubahan Biodata Anggota";

    public final static String DISPLAY_KTP_BARU = "KTP Baru";
    public final static String DISPLAY_KTP_HILANG_RUSAK = "KTP Hilang / Rusak";
    public final static String DISPLAY_KTP_SALAH_DIRUBAH = "KTP Salah / Dirubah";

    public final static String DISPLAY_AKTA_KELAHIRAN = "Akta Kelahiran";
    public final static String DISPLAY_AKTA_KEMATIAN = "Akta Kematian";
    public final static String DISPLAY_AKTA_PERKAWINAN = "Akta Perkawinan";
    public final static String DISPLAY_PENCATATAN_PERUBAHAN_NAMA = "Pencatatan Perubahan Nama";
    public final static String DISPLAY_PENCATATAN_PENGAKUAN_ANAK = "Pencatatan Pengakuan Anak";
    public final static String DISPLAY_KUTIPAN_KEDUA_AKTA_PENCATATAN_SIPIL = "Kutipan Kedua Akta Pencatatan Sipil";
    public final static String DISPLAY_ADOPSI = "Pencatatan Pengangkatan Anak (Adopsi)";
    public final static String DISPLAY_AKTA_PENGESAHAN_ANAK = "Pencatatan dan Penerbitan Akta Pengesahan Anak";
    //</editor-fold>

    //</editor-fold>

    //<editor-fold desc="PERIZINAN">

    //<editor-fold desc="VALUE">
    public final static String IMB = "IMB";
    public final static String IZIN_GANGGUAN = "IZIN_GANGGUAN";
    public final static String IZIN_TONTONAN = "IZIN_TONTONAN";
    public final static String SIUP = "SIUP";
    public final static String IZIN_TOKO_MODERN = "IZIN_TOKO_MODERN";
    //</editor-fold>

    //<editor-fold desc="DISPLAY">
    public final static String DISPLAY_IMB = "IMB";
    public final static String DISPLAY_IZIN_GANGGUAN = "Izin Gangguan";
    public final static String DISPLAY_IZIN_TONTONAN = "Izin Tontonan";
    public final static String DISPLAY_SIUP = "SIUP";
    public final static String DISPLAY_IZIN_TOKO_MODERN = "Izin Toko Modern";
    //</editor-fold>

    //</editor-fold>

    //<editor-fold desc="KESEHATAN">

    //<editor-fold desc="LIST FITUR">
    public final static String RUMAH_SAKIT = "RUMAH_SAKIT";
    public final static String PUSKESMAS_UTAMA = "PUSKESMAS_UTAMA";
    //</editor-fold>

    //<editor-fold desc="LIST FITUR + VALUE">
    public final static String KLINIK = "KLINIK";
    public final static String APOTEK = "APOTEK";
    public final static String DOKTER_GIGI = "DOKTER_GIGI";
    public final static String DOKTER_UMUM = "DOKTER_UMUM";
    public final static String LABORATORIUM = "LABORATORIUM";
    public final static String OPTIK = "OPTIK";

    public final static String RUMAH_SAKIT_UMUM = "RUMAH_SAKIT_UMUM";
    public final static String RUMAH_SAKIT_IBU_DAN_ANAK = "RUMAH_SAKIT_IBU_DAN_ANAK";
    public final static String RUMAH_SAKIT_BERSALIN = "RUMAH_SAKIT_BERSALIN";

    public final static String PUSKESMAS = "PUSKESMAS";
    public final static String PUSKESMAS_UPT = "PUSKESMAS_UPT";
    //</editor-fold>

    //<editor-fold desc="DISPLAY">
    public final static String DISPLAY_RUMAH_SAKIT = "Rumah Sakit Kota Malang";
    public final static String DISPLAY_PUSKESMAS_UTAMA = "Puskesmas Kota Malang";
    public final static String DISPLAY_KLINIK = "Klinik Kota Malang";
    public final static String DISPLAY_APOTEK = "Apotek Kota Malang";
    public final static String DISPLAY_DOKTER_GIGI = "Dokter Gigi Kota Malang";
    public final static String DISPLAY_LABORATORIUM = "Laboratorium Kota Malang";
    public final static String DISPLAY_OPTIK = "Optik Kota Malang";

    public final static String DISPLAY_RUMAH_SAKIT_UMUM = "Rumah Sakit Umum";
    public final static String DISPLAY_RUMAH_SAKIT_IBU_DAN_ANAK = "Rumah Sakit Ibu dan Anak";
    public final static String DISPLAY_RUMAH_SAKIT_BERSALIN = "Rumah Sakit Bersalin";

    public final static String DISPLAY_PUSKESMAS = "Puskesmas";
    public final static String DISPLAY_PUSKESMAS_UPT = "Puskesmas UPT Pembantu";

    //</editor-fold>

    //</editor-fold>

    //<editor-fold desc="PAJAK">
    //<editor-fold desc="Value">
    public final static String PAJAK_DAERAH = "PAJAK_DAERAH";
    public final static String PAJAK_HOTEL = "PAJAK_HOTEL";
    public final static String PAJAK_RESTORAN = "PAJAK_RESTORAN";
    public final static String PAJAK_HIBURAN = "PAJAK_HIBURAN";
    public final static String PAJAK_REKLAME = "PAJAK_REKLAME";
    public final static String PAJAK_PENERANGAN_JALAN = "PAJAK_PENERANGAN_JALAN";
    public final static String PAJAK_PARKIR = "PAJAK_PARKIR";
    public final static String PAJAK_AIR_TANAH = "PAJAK_AIR_TANAH";
    public final static String PAJAK_BUMI_BANGUNAN = "PAJAK_BUMI_BANGUNAN";
    public final static String BPHTB = "BPHTB";
    //</editor-fold>

    //<editor-fold desc="Display">
    public final static String DISPLAY_PAJAK_DAERAH = "Deskripsi Pajak Daerah";
    public final static String DISPLAY_PAJAK_HOTEL = "Pajak Hotel";
    public final static String DISPLAY_PAJAK_RESTORAN = "Pajak Restoran";
    public final static String DISPLAY_PAJAK_HIBURAN = "Pajak Hiburan";
    public final static String DISPLAY_PAJAK_REKLAME = "Pajak Reklame";
    public final static String DISPLAY_PAJAK_PENERANGAN_JALAN = "Pajak Penerangan Jalan";
    public final static String DISPLAY_PAJAK_PARKIR = "Pajak Parkir";
    public final static String DISPLAY_PAJAK_AIR_TANAH = "Pajak Air Tanah";
    public final static String DISPLAY_PAJAK_BUMI_BANGUNAN = "Pajak Bumi dan Bangunan";
    public final static String DISPLAY_BPHTB = "BPHTB";
    //</editor-fold>
    //</editor-fold>

    //<editor-fold desc="PARIWISATA">
    public final static String KULINER_OLEH_OLEH = "KULINER_OLEH_OLEH";
    public final static String INFO_WISATA = "INFO_WISATA";
    public final static String INFO_HOTEL = "hotel";
    public final static String OLEH_OLEH = "OLEH_OLEH";

    public final static String INFO_SEJARAH = "sejarah";
    public final static String INFO_EDUKASI = "edukasi";
    public final static String INFO_REKREASI = "rekreasi";
    public final static String INFO_BELANJA = "belanja";
    public final static String INFO_RELIGI = "religi";
    public final static String INFO_KAMPUNG = "kampung";
    public final static String INFO_TAMAN_HUTAN = "taman_hutan";
    public final static String INFO_OLAHRAGA = "olahraga";

    public final static String INFO_HOTEL2 = "INFO_HOTEL2";
    public final static String INFO_HOMESTAY = "homestay";
    public final static String INFO_GUESTHOUSE = "guesthouse";

    public final static String INFO_OLEH_OLEH = "oleh_oleh";
    public final static String INFO_KULINER = "kuliner";
    //</editor-fold>

    //<editor-fold desc="TANGGAP">
    public final static String LOKER_ID_LOWONGAN = "LOKER_ID_LOWONGAN";
    public final static String LOKER_NAMA_POSISI = "LOKER_NAMA_POSISI";
    public final static String LOKER_NAMA_PERUSAHAAN = "LOKER_NAMA_PERUSAHAAN";
    public final static String LOKER_KATEGORI_PRODI = "LOKER_KATEGORI_PRODI";
    public final static String LOKER_PERSYARATAN = "LOKER_PERSYARATAN";
    public final static String LOKER_KUALIFIKASI_SKILL = "LOKER_KUALIFIKASI_SKILL";
    public final static String LOKER_DESKRIPSI = "LOKER_DESKRIPSI";
    public final static String LOKER_GAJI = "LOKER_GAJI";
    public final static String LOKER_BATAS_WAKTU = "LOKER_BATAS_WAKTU";
    //</editor-fold>

    //Sumber
    public final static String SUMBER_DISPENDUK = "Sumber : dispendukcapil.malangkota.go.id";
    public final static String SUMBER_PERIJINAN = "Sumber : dpmptsp.malangkota.go.id";
    public final static String SUMBER_PAJAK = "Sumber : Badan Pelayanan Pajak Daerah Kota Malang";

    //Sub Title Pengurusan Kartu Keluarga
    public final static String SUBTITLE_KK_BARU = "Persyaratan Mengurus Kartu Keluarga Baru";
    public final static String SUBTITLE_KK_HILANG = "Persyaratan Mengurus Kartu Keluarga Hilang/Rusak";
    public final static String SUBTITLE_KK_TAMBAH_ANGGOTA = "Persyaratan Perubahan Kartu Kleuarga karena Penambahan Anggota";
    public final static String SUBTITLE_KK_PENGURANGAN_ANGGOTA = "Persyaratan Mengurus Perubahan Kartu Keluarga karena Pengurangan Anggota";
    public final static String SUBTITLE_KK_PERUBAHAN_ANGGOTA = "Persyaratan Mengurus Perubahan Kartu Keluarga karena Perubahan Biodata Anggota";

    //Sub Title Pengurusan Akta
    public final static String SUBTITLE_AKTA_KELAHIRAN = "Persyaratan Mengurus Akta Kelahiran";
    public final static String SUBTITLE_AKTA_KEMATIAN = "Persyaratan Mengurus Akta Kematian";
    public final static String SUBTITLE_AKTA_PERKAWINAN = "Persyaratan Mengurus Akta";
    public final static String SUBTITLE_AKTA_PENCATATAN_PERUBAHAN_NAMA = "Persyaratan Mengurus Akta Perubahan Nama";
    public final static String SUBTITLE_AKTA_PENCATATAN_PENGAKUAN_ANAK = "Persyaratan Mengurus Pecatatan dan Penerbitan Akta Pengesahan Anak";
    public final static String SUBTITLE_AKTA_KUTIPAN_KEDUA_AKTA_PENCATATAN_SIPIL = "Persyaratan Mengurus Kutipan Kedua Akta Pencatatan Sipil";
    public final static String SUBTITLE_AKTA_PENCATATAN_PENGANGKATAN_ANAK = "Persyaratan Mengurus Pencatatan Pengangkatan Anak(Adopsi)";
    public final static String SUBTITLE_AKTA_PENGESAHAN_ANAK = "Persyaratan Mengurus Akta Pengesahan Anak";

    //Sub Tittle Pengurusan KTP

    public final static String SUBTITLE_KTP_BARU = "Persyaratan Mengurus KTP Baru";
    public final static String SUBTITLE_KTP_HILANG = "Persyaratan Mengurus KTP Hilang/Rusak";
    public final static String SUBTITLE_KTP_SALAH = "Persyaratan Mengurus Perubahan Kartu Keluarga karena Pengurangan Anggota";

    //Sub Tittle PAJAK
    public final static String SUBTITLE_PAJAK_HOTEL = "Pajak Hotel";
    public final static String SUBTITLE_PAJAK_RESTORAN = "Pajak Restoran";
    public final static String SUBTITLE_PAJAK_HIBURAN = "Pajak Hiburan";
    public final static String SUBTITLE_PAJAK_REKLAME = "Pajak Reklame";
    public final static String SUBTITLE_PAJAK_PENERANGAN_JALAN = "Pajak Penerangan Jalan";
    public final static String SUBTITLE_PAJAK_PARKIR = "Pajak Parkir";
    public final static String SUBTITLE_PAJAK_AIR_TANAH = "Pajak Air Tanah";
    public final static String SUBTITLE_PAJAK_BPHTB = "Bea Perolehan Hak Atas Tanah dan Bangunan";
    public final static String SUBTITLE_PAJAK_BUMI = "Pajak Bumi dan Bangunan";

    //Sub Tittle IMB
    public final static String SUBTITLE_PERIJINAN_IMB = "Persyaratan Mengurus IMB (Izin Mendirikan Bagunan)";
    public final static String SUBTITLE_PERIJINAN_IZIN_GANGGUAN = "Persyaratan Mengurus Izin Gangguan";
    public final static String SUBTITLE_PERIJINAN_TONTONAN = "Persyaratan Mengurus Tontonan";
    public final static String SUBTITLE_PERIJINAN_SIUP = "Persyaratan Mengurus SIUP";
    public final static String SUBTITLE_PERIJINAN_IZIN_TOKO = "Persyaratan Mengurus Izin Toko Modern";

    public final static String EMPTY = "";

}
