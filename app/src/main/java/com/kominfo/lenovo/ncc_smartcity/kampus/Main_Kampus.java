package com.kominfo.lenovo.ncc_smartcity.kampus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.kominfo.lenovo.ncc_smartcity.R;

public class Main_Kampus extends Activity {

    CardView cv_negeri, cv_swasta, cv_politeknik, cv_st, cv_is;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_kampus);

        cv_negeri = findViewById(R.id.cv_negeri);
        cv_swasta = findViewById(R.id.cv_swasta);
        cv_politeknik = findViewById(R.id.cv_politeknik);
        cv_st = findViewById(R.id.cv_st);
        cv_is = findViewById(R.id.cv_is);

        cv_negeri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Kampus.this, KampusNegeri.class);
                startActivity(i);
            }
        });

        cv_swasta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Kampus.this, KampusSwasta.class);
                startActivity(i);
            }
        });

        cv_politeknik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Kampus.this, KampusPoliteknik.class);
                startActivity(i);
            }
        });

        cv_st.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Kampus.this, KampusSekolah.class);
                startActivity(i);
            }
        });

        cv_is.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Kampus.this, KampusInstitut.class);
                startActivity(i);
            }
        });
    }
}
