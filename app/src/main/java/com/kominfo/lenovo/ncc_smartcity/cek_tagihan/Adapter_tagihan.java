package com.kominfo.lenovo.ncc_smartcity.cek_tagihan;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kominfo.lenovo.ncc_smartcity.R;

import java.util.ArrayList;

public class Adapter_tagihan extends RecyclerView.Adapter<Adapter_tagihan.ItemViewHolder> {

    private ArrayList<Model_tagihan> dataList;
    private Context context;

    public Adapter_tagihan(Context context) {
        this.context = context;
    }

    public Adapter_tagihan(ArrayList<Model_tagihan> dataList) {
        this.dataList = dataList;
    }

    @Override
    public Adapter_tagihan.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.cek_tagihan_pbb_recycler, parent, false);
        return new Adapter_tagihan.ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Adapter_tagihan.ItemViewHolder holder, final int position) {
        Log.e("holder", "onBindViewHolder: " + holder);
        holder.tahun.setText(dataList.get(position).getKey());
        holder.nominal.setText(dataList.get(position).getNominal());

    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tahun,nominal;

        public ItemViewHolder(View itemView) {
            super(itemView);
            tahun = itemView.findViewById(R.id.txt_tahun);
            nominal = itemView.findViewById(R.id.txt_nominal);

        }


    }
}
