package com.kominfo.lenovo.ncc_smartcity.surat;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.kominfo.lenovo.ncc_smartcity.R;

public class Main_Sim extends Activity {

    TextView textView;
    Typeface customFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sim);

        customFont = Typeface.createFromAsset(getAssets(), "font/MontserratBold.ttf");
        textView = findViewById(R.id.textViewsim);
        textView.setTypeface(customFont);
    }
}
