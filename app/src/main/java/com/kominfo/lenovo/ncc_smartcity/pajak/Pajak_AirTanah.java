package com.kominfo.lenovo.ncc_smartcity.pajak;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.kominfo.lenovo.ncc_smartcity.R;

public class Pajak_AirTanah extends Activity {

    TextView textView;
    Typeface customFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pajakairtanah);

        customFont = Typeface.createFromAsset(getAssets(), "font/MontserratBold.ttf");
        textView = findViewById(R.id.textViewair);
        textView.setTypeface(customFont);
    }
}
